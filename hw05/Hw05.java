////////////////////
// Jannah Wing
// CSE 002
// HW #5
// Due October 9, 2018
// 
// Calculates the probability of poker hands
//
// Input variables: given by user
//     Number of hands/ simulations to be run
//
// Output variables: printed comments on each input 
//     Probability for drawing four of a kind
//     Probability for drawing three of a kind
//     Probability for drawing two pairs
//     Probability for drawing one pair

// import Scanner class 
import java.util.Scanner; 
// define a class
public class Hw05 {
  // add main method required for every Java program
  public static void main(String [] args) {
    
    // declare an instance of the Scanner object 
    Scanner myScanner = new Scanner (System.in);
    
    // printed statement asking user to input the number of times the program should generate hands 
    System.out.print("Enter the number of hands you would like to generate (as integer): ");
    
    // check that input is integer, and if not, let user reenter correctly 
    // initialize boolean and integer used in loop 
    boolean handsWantedCheck = false; 
    int handsWanted = -1;
    // use loop to let user input answer, check that it is correct, and reenter if necessary  
    while (!handsWantedCheck) { // while incorrect input (handsWanted is not integer)
      // check that user input in correct form
      handsWantedCheck = myScanner.hasNextInt(); // will return true if user types int and false if not 
      if (handsWantedCheck){ // if correct form
        handsWanted = myScanner.nextInt(); 
      } // closes if
      else if (!handsWantedCheck) { // if incorrect form
        myScanner.next(); // discards incorrect input 
        System.out.println("Incorrect Input, please try again.");
        System.out.print("Enter the number of hands you would like to generate (as integer): ");
      } // closes else if
    } // closes while
    
    // initialize variables which need to be used outside of loop (counts of desired hands)
    int fourOfAKindCount = 0;
    int threeOfAKindCount = 0; 
    int twoPairsCount = 0;
    int onePairCount = 0; 
    
    // Loop for each hand, choosing unique card, checking if desired hand, and counting hands 
    int handsGenerated = 1; // initializes hands generated (aka trial/hand 1) 
    while (handsGenerated <= handsWanted) { // runs through loop the amount of times hands wanted 
      
      // get five random numbers between 1 and 52 (no repeats)
      
      // card 1
      // get random integer in range [1,53) (1 to 52 including 52) 
      int card1Number = (int) (Math.random()*52) +1; 
    
      // card 2
      // get random integer in range [1,53) (1 to 52 including 52) and check that unique to hand
      // initialize boolean and integer used in loop 
      boolean card2NumberCheck = true; 
      int card2Number = -1;
      // use loop to generate card number, check that hasn't been choosen before, and if so generate again  
      while (card2NumberCheck) { // while card2Number = card1Number
        int checkValueCard2 = (int) (Math.random()*52) +1; // gets new random bumber
        card2NumberCheck = (checkValueCard2 == card1Number); // true if card 1 is same as card 2
        if (!card2NumberCheck){ // if card2 is not equal to card 1
          card2Number = checkValueCard2; // defines card 2 as new random number which has already been checked 
        } // closes if
      } // closes while
    
      // card 3
      // get random integer in range [1,53) (1 to 52 including 52) and check that unique to hand
      // initialize boolean and integer used in loop 
      boolean card3NumberCheck = true; 
      int card3Number = -1;
      // use loop to generate card number, check that hasn't been choosen before, and if so generate again  
      while (card3NumberCheck) { // while card3Number = card1Number or card2Number
        int checkValueCard3 = (int) (Math.random()*52) +1; // gets new random bumber
        card3NumberCheck = ((checkValueCard3 == card1Number) || (checkValueCard3 == card2Number) ); // true if card 1 is same as card 2
        if (!card3NumberCheck){ // if card2 is not equal to card 1 or card 2
          card3Number = checkValueCard3; // defines card 3 as new random number which has already been checked 
        } // closes if
      } // closes while   
    
      // card 4
      // get random integer in range [1,53) (1 to 52 including 52) and check that unique to hand
      // initialize boolean and integer used in loop 
      boolean card4NumberCheck = true; 
      int card4Number = -1;
      // use loop to generate card number, check that hasn't been choosen before, and if so generate again  
      while (card4NumberCheck) { // while card3Number = card1Number or card2Number or card 3
        int checkValueCard4 = (int) (Math.random()*52) +1; // gets new random bumber
        card4NumberCheck = ((checkValueCard4 == card1Number) || (checkValueCard4 == card2Number) || (checkValueCard4 == card3Number)); // true if card 4 is same as card 1/2/3
        if (!card4NumberCheck){ // if card4 is not equal to card 1,2,3
          card4Number = checkValueCard4; // defines card 4 as new random number which has already been checked 
        } // closes if
      } // closes while 
    
      // card 5
      // get random integer in range [1,53) (1 to 52 including 52) and check that unique to hand
      // initialize boolean and integer used in loop 
      boolean card5NumberCheck = true; 
      int card5Number = -1;
      // use loop to generate card number, check that hasn't been choosen before, and if so generate again  
      while (card5NumberCheck) { // while card5Number = card1Number or 2/3/4
        int checkValueCard5 = (int) (Math.random()*52) +1; // gets new random bumber
        card5NumberCheck = ((checkValueCard5 == card1Number) || (checkValueCard5 == card2Number) || (checkValueCard5 == card3Number) || (checkValueCard5 == card4Number)); // true if card 4 is same as card 1/2/3/4
        if (!card5NumberCheck){ // if card5 is not equal to card 1,2,3,4
          card5Number = checkValueCard5; // defines card 5 as new random number which has already been checked 
        } // closes if
      } // closes while 
      
      // simplifies cards for by defining each card based on number/face value
      // initializes variables used for checking pairs 
      int card1Pair = 80, 
      card2Pair = 81, 
      card3Pair = 82, 
      card4Pair = 83, 
      card5Pair = 84; 
      
      // redefining card1Number based on face value 
      if ((card1Number == 1) || (card1Number == 14) || (card1Number == 27) || (card1Number == 40)) {
        card1Pair = 1; // defines cards 1, 14, 27, and 40 as Ace
      }
      else if ((card1Number == 2) || (card1Number == 15) || (card1Number == 28) || (card1Number == 41)) {
        card1Pair = 2; // defines cards 2, 15, 28 and 41 as 2
      }
      else if ((card1Number == 3) || (card1Number == 16) || (card1Number == 29) || (card1Number == 42)) {
        card1Pair = 3; // defines cards 3, 16, 29 and 42 as 3
      }
      else if ((card1Number == 4) || (card1Number == 17) || (card1Number == 30) || (card1Number == 43)) {
        card1Pair = 4; // defines cards 4, 17, 30 and 43 as 4
      }
      else if ((card1Number == 5) || (card1Number == 18) || (card1Number == 31) || (card1Number == 44)) {
        card1Pair = 5; // defines cards 5, 18, 31, 44 as 5
      }
      else if ((card1Number == 6) || (card1Number == 19) || (card1Number == 32) || (card1Number == 45)) {
        card1Pair = 6; // defines cards 6, 19, 32, 45 as 6
      }
      else if ((card1Number == 7) || (card1Number == 20) || (card1Number == 33) || (card1Number == 46)) {
        card1Pair = 7; // defines cards 7, 20, 33, 46 as 7
      }
      else if ((card1Number == 8) || (card1Number == 21) || (card1Number == 34) || (card1Number == 47)) {
        card1Pair = 8; // defines cards 8, 21, 34, 47 as 8
      }
      else if ((card1Number == 9) || (card1Number == 22) || (card1Number == 35) || (card1Number == 48)) {
        card1Pair = 9; // defines cards 9, 22, 35, 48 as 9
      }
      else if ((card1Number == 10) || (card1Number == 23) || (card1Number == 36) || (card1Number == 49)) {
        card1Pair = 10; // defines cards 10, 23, 36, 49 as 10
      }
      else if ((card1Number == 11) || (card1Number == 24) || (card1Number == 37) || (card1Number == 50)) {
        card1Pair = 11; // defines cards 11, 24, 37, 50 as Jack
      }
      else if ((card1Number == 12) || (card1Number == 25) || (card1Number == 38) || (card1Number == 51)) {
        card1Pair = 12; // defines cards 12, 25, 38, 51 as Queen
      }
      else if ((card1Number == 13) || (card1Number == 26) || (card1Number == 39) || (card1Number == 52)) {
        card1Pair = 13; // defines cards 13, 26, 39, 52 as King
      }
      // redefining card2Number based on face value 
      if ((card2Number == 1) || (card2Number == 14) || (card2Number == 27) || (card2Number == 40)) {
        card2Pair = 1; // defines cards 1, 14, 27, and 40 as Ace
      }
      else if ((card2Number == 2) || (card2Number == 15) || (card2Number == 28) || (card2Number == 41)) {
        card2Pair = 2; // defines cards 2, 15, 28 and 41 as 2
      }
      else if ((card2Number == 3) || (card2Number == 16) || (card2Number == 29) || (card2Number == 42)) {
        card2Pair = 3; // defines cards 3, 16, 29 and 42 as 3
      }
      else if ((card2Number == 4) || (card2Number == 17) || (card2Number == 30) || (card2Number == 43)) {
        card2Pair = 4; // defines cards 4, 17, 30 and 43 as 4
      }
      else if ((card2Number == 5) || (card2Number == 18) || (card2Number == 31) || (card2Number == 44)) {
        card2Pair = 5; // defines cards 5, 18, 31, 44 as 5
      }
      else if ((card2Number == 6) || (card2Number == 19) || (card2Number == 32) || (card2Number == 45)) {
        card2Pair = 6; // defines cards 6, 19, 32, 45 as 6
      }
      else if ((card2Number == 7) || (card2Number == 20) || (card2Number == 33) || (card2Number == 46)) {
        card2Pair = 7; // defines cards 7, 20, 33, 46 as 7
      }
      else if ((card2Number == 8) || (card2Number == 21) || (card2Number == 34) || (card2Number == 47)) {
        card2Pair = 8; // defines cards 8, 21, 34, 47 as 8
      }
      else if ((card2Number == 9) || (card2Number == 22) || (card2Number == 35) || (card2Number == 48)) {
        card2Pair = 9; // defines cards 9, 22, 35, 48 as 9
      }
      else if ((card2Number == 10) || (card2Number == 23) || (card2Number == 36) || (card2Number == 49)) {
        card2Pair = 10; // defines cards 10, 23, 36, 49 as 10
      }
      else if ((card2Number == 11) || (card2Number == 24) || (card2Number == 37) || (card2Number == 50)) {
        card2Pair = 11; // defines cards 11, 24, 37, 50 as Jack
      }
      else if ((card2Number == 12) || (card2Number == 25) || (card2Number == 38) || (card2Number == 51)) {
        card2Pair = 12; // defines cards 12, 25, 38, 51 as Queen
      }
      else if ((card2Number == 13) || (card2Number == 26) || (card2Number == 39) || (card2Number == 52)) {
        card2Pair = 13; // defines cards 13, 26, 39, 52 as King
      }
      // redefining card3Number based on face value 
      if ((card3Number == 1) || (card3Number == 14) || (card3Number == 27) || (card3Number == 40)) {
        card3Pair = 1; // defines cards 1, 14, 27, and 40 as Ace
      }
      else if ((card3Number == 2) || (card3Number == 15) || (card3Number == 28) || (card3Number == 41)) {
        card3Pair = 2; // defines cards 2, 15, 28 and 41 as 2
      }
      else if ((card3Number == 3) || (card3Number == 16) || (card3Number == 29) || (card3Number == 42)) {
        card3Pair = 3; // defines cards 3, 16, 29 and 42 as 3
      }
      else if ((card3Number == 4) || (card3Number == 17) || (card3Number == 30) || (card3Number == 43)) {
        card3Pair = 4; // defines cards 4, 17, 30 and 43 as 4
      }
      else if ((card3Number == 5) || (card3Number == 18) || (card3Number == 31) || (card3Number == 44)) {
        card3Pair = 5; // defines cards 5, 18, 31, 44 as 5
      }
      else if ((card3Number == 6) || (card3Number == 19) || (card3Number == 32) || (card3Number == 45)) {
        card3Pair = 6; // defines cards 6, 19, 32, 45 as 6
      }
      else if ((card3Number == 7) || (card3Number == 20) || (card3Number == 33) || (card3Number == 46)) {
        card3Pair = 7; // defines cards 7, 20, 33, 46 as 7
      }
      else if ((card3Number == 8) || (card3Number == 21) || (card3Number == 34) || (card3Number == 47)) {
        card3Pair = 8; // defines cards 8, 21, 34, 47 as 8
      }
      else if ((card3Number == 9) || (card3Number == 22) || (card3Number == 35) || (card3Number == 48)) {
        card3Pair = 9; // defines cards 9, 22, 35, 48 as 9
      }
      else if ((card3Number == 10) || (card3Number == 23) || (card3Number == 36) || (card3Number == 49)) {
        card3Pair = 10; // defines cards 10, 23, 36, 49 as 10
      }
      else if ((card3Number == 11) || (card3Number == 24) || (card3Number == 37) || (card3Number == 50)) {
        card3Pair = 11; // defines cards 11, 24, 37, 50 as Jack
      }
      else if ((card3Number == 12) || (card3Number == 25) || (card3Number == 38) || (card3Number == 51)) {
        card3Pair = 12; // defines cards 12, 25, 38, 51 as Queen
      }
      else if ((card3Number == 13) || (card3Number == 26) || (card3Number == 39) || (card3Number == 52)) {
        card3Pair = 13; // defines cards 13, 26, 39, 52 as King
      }
      // redefining card4Number based on face value 
      if ((card4Number == 1) || (card4Number == 14) || (card4Number == 27) || (card4Number == 40)) {
        card4Pair = 1; // defines cards 1, 14, 27, and 40 as Ace
      }
      else if ((card4Number == 2) || (card4Number == 15) || (card4Number == 28) || (card4Number == 41)) {
        card4Pair = 2; // defines cards 2, 15, 28 and 41 as 2
      }
      else if ((card4Number == 3) || (card4Number == 16) || (card4Number == 29) || (card4Number == 42)) {
        card4Pair = 3; // defines cards 3, 16, 29 and 42 as 3
      }
      else if ((card4Number == 4) || (card4Number == 17) || (card4Number == 30) || (card4Number == 43)) {
        card4Pair = 4; // defines cards 4, 17, 30 and 43 as 4
      }
      else if ((card4Number == 5) || (card4Number == 18) || (card4Number == 31) || (card4Number == 44)) {
        card4Pair = 5; // defines cards 5, 18, 31, 44 as 5
      }
      else if ((card4Number == 6) || (card4Number == 19) || (card4Number == 32) || (card4Number == 45)) {
        card4Pair = 6; // defines cards 6, 19, 32, 45 as 6
      }
      else if ((card4Number == 7) || (card4Number == 20) || (card4Number == 33) || (card4Number == 46)) {
        card4Pair = 7; // defines cards 7, 20, 33, 46 as 7
      }
      else if ((card4Number == 8) || (card4Number == 21) || (card4Number == 34) || (card4Number == 47)) {
        card4Pair = 8; // defines cards 8, 21, 34, 47 as 8
      }
      else if ((card4Number == 9) || (card4Number == 22) || (card4Number == 35) || (card4Number == 48)) {
        card4Pair = 9; // defines cards 9, 22, 35, 48 as 9
      }
      else if ((card4Number == 10) || (card4Number == 23) || (card4Number == 36) || (card4Number == 49)) {
        card4Pair = 10; // defines cards 10, 23, 36, 49 as 10
      }
      else if ((card4Number == 11) || (card4Number == 24) || (card4Number == 37) || (card4Number == 50)) {
        card4Pair = 11; // defines cards 11, 24, 37, 50 as Jack
      }
      else if ((card4Number == 12) || (card4Number == 25) || (card4Number == 38) || (card4Number == 51)) {
        card4Pair = 12; // defines cards 12, 25, 38, 51 as Queen
      }
      else if ((card4Number == 13) || (card4Number == 26) || (card4Number == 39) || (card4Number == 52)) {
        card4Pair = 13; // defines cards 13, 26, 39, 52 as King
      }
      // redefining card5Number based on face value 
      if ((card5Number == 1) || (card5Number == 14) || (card5Number == 27) || (card5Number == 40)) {
        card5Pair = 1; // defines cards 1, 14, 27, and 40 as Ace
      }
      else if ((card5Number == 2) || (card5Number == 15) || (card5Number == 28) || (card5Number == 41)) {
        card5Pair = 2; // defines cards 2, 15, 28 and 41 as 2
      }
      else if ((card5Number == 3) || (card5Number == 16) || (card5Number == 29) || (card5Number == 42)) {
        card5Pair = 3; // defines cards 3, 16, 29 and 42 as 3
      }
      else if ((card5Number == 4) || (card5Number == 17) || (card5Number == 30) || (card5Number == 43)) {
        card5Pair = 4; // defines cards 4, 17, 30 and 43 as 4
      }
      else if ((card5Number == 5) || (card5Number == 18) || (card5Number == 31) || (card5Number == 44)) {
        card5Pair = 5; // defines cards 5, 18, 31, 44 as 5
      }
      else if ((card5Number == 6) || (card5Number == 19) || (card5Number == 32) || (card5Number == 45)) {
        card5Pair = 6; // defines cards 6, 19, 32, 45 as 6
      }
      else if ((card5Number == 7) || (card5Number == 20) || (card5Number == 33) || (card5Number == 46)) {
        card5Pair = 7; // defines cards 7, 20, 33, 46 as 7
      }
      else if ((card5Number == 8) || (card5Number == 21) || (card5Number == 34) || (card5Number == 47)) {
        card5Pair = 8; // defines cards 8, 21, 34, 47 as 8
      }
      else if ((card5Number == 9) || (card5Number == 22) || (card5Number == 35) || (card5Number == 48)) {
        card5Pair = 9; // defines cards 9, 22, 35, 48 as 9
      }
      else if ((card5Number == 10) || (card5Number == 23) || (card5Number == 36) || (card5Number == 49)) {
        card5Pair = 10; // defines cards 10, 23, 36, 49 as 10
      }
      else if ((card5Number == 11) || (card5Number == 24) || (card5Number == 37) || (card5Number == 50)) {
        card5Pair = 11; // defines cards 11, 24, 37, 50 as Jack
      }
      else if ((card5Number == 12) || (card5Number == 25) || (card5Number == 38) || (card5Number == 51)) {
        card5Pair = 12; // defines cards 12, 25, 38, 51 as Queen
      }
      else if ((card5Number == 13) || (card5Number == 26) || (card5Number == 39) || (card5Number == 52)) {
        card5Pair = 13; // defines cards 13, 26, 39, 52 as King
      }
      
      // check if hand is one of 4 chosen hands 
      
      // check if four of a kind 
      if ((card1Pair == card2Pair) && (card1Pair == card3Pair) && (card1Pair == card4Pair) && (card1Pair != card5Pair)){ // if card 1,2,3,4 are same Pair
        fourOfAKindCount++; // saves card as four of a kind
      } // closes if card 1,2,3,4 are same Pair
      else if ((card1Pair != card2Pair) && (card1Pair == card3Pair) && (card1Pair == card4Pair) && (card1Pair == card5Pair)){ // if card 1,3,4,5 are same Pair
        fourOfAKindCount++; // saves card as four of a kind
      } // closes if card 1,3,4,5 are same Pair
      else if ((card1Pair == card2Pair) && (card1Pair != card3Pair) && (card1Pair == card4Pair) && (card1Pair == card5Pair)){ // if card 1,2,4,5 are same Pair 
        fourOfAKindCount++; // saves card as four of a kind
      } // closes if card 1,2,4,5 are same Pair 
      else if ((card1Pair == card2Pair) && (card1Pair == card3Pair) && (card1Pair != card4Pair) && (card1Pair == card5Pair)){ // if card 1,2,3,5 are same Pair 
        fourOfAKindCount++; // saves card as four of a kind
      } // closes if card 1,2,3,5 are same Pair 
      else if ((card2Pair != card1Pair) && (card2Pair == card3Pair) && (card2Pair == card4Pair) && (card2Pair == card5Pair)){ // if card 2,3,4,5 are same Pair 
        fourOfAKindCount++; // saves card as four of a kind
      } // closes if card 2,3,4,5 are same Pair 
      
      // check if three of a kind
      else if ((card1Pair == card2Pair) && (card1Pair == card3Pair) && (card1Pair != card4Pair) && (card1Pair != card5Pair)){ // if cards 1,2,3 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if card 1,2,3 are same Pair
      else if ((card1Pair == card2Pair) && (card1Pair != card3Pair) && (card1Pair == card4Pair) && (card1Pair != card5Pair)){ // if cards 1,2,4 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if card 1,2,4 are same Pair 
      else if ((card1Pair == card2Pair) && (card1Pair != card3Pair) && (card1Pair != card4Pair) && (card1Pair == card5Pair)){ // if cards 1,2,5 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if cards 1,2,5 are same Pair
      else if ((card1Pair != card2Pair) && (card1Pair == card3Pair) && (card1Pair == card4Pair) && (card1Pair != card5Pair)){ // if cards 1,3,4 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if cards 1,3,4 are same Pair 
      else if ((card1Pair != card2Pair) && (card1Pair == card3Pair) && (card1Pair != card4Pair) && (card1Pair == card5Pair)){ // if cards 1,3,5 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if cards 1,3,5 are same Pair 
      else if ((card1Pair != card2Pair) && (card1Pair != card3Pair) && (card1Pair == card4Pair) && (card1Pair == card5Pair)){ // if cards 1,4,5 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if cards 1,4,5 are same Pair 
      else if ((card2Pair != card1Pair) && (card2Pair == card3Pair) && (card2Pair == card4Pair) && (card2Pair != card5Pair)){ // if cards 2,3,4 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if cards 2,3,4 are same Pair 
      else if ((card2Pair != card1Pair) && (card2Pair == card3Pair) && (card2Pair != card4Pair) && (card2Pair == card5Pair)){ // if cards 2,3,5 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if cards 2,3,5 are same Pair
      else if ((card2Pair != card1Pair) && (card2Pair != card3Pair) && (card2Pair == card4Pair) && (card2Pair == card5Pair)){ // if cards 2,4,5 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if cards 2,4,5 are same Pair 
      else if ((card3Pair != card1Pair) && (card3Pair != card2Pair) && (card3Pair == card4Pair) && (card3Pair == card5Pair)){ // if cards 3,4,5 are same Pair 
        threeOfAKindCount++; // saves card as three of a kind
      } // closes if cards 3,4,5 are same Pair 
      
      // checks if two pairs 
      if (((card1Pair == card2Pair) && (card3Pair == card4Pair)) || ((card1Pair == card2Pair) && (card3Pair == card5Pair)) || ((card1Pair == card2Pair) && (card4Pair == card5Pair))) { // if same face value for cards 1/2 and 3/4, 4/5, 3/5
        twoPairsCount++; // saves card as two pairs  
      } // closes if same face value for cards 1/2 and 3/4, 4/5, 3/5
      else if (((card2Pair == card3Pair) && (card1Pair == card4Pair)) || ((card2Pair == card3Pair) && (card1Pair == card5Pair)) || ((card2Pair == card3Pair) && (card4Pair == card5Pair))) { // if same face value for cards 2/3 and 1/4, 4/5, 1/5
        twoPairsCount++; // saves card as two pairs  
      } // closes if same face value for cards 2/3 and 1/4, 4/5, 1/5
      else if (((card3Pair == card4Pair) && (card1Pair == card5Pair)) || ((card3Pair == card4Pair) && (card2Pair == card5Pair))) { // if same face value for cards 3/4 and 1/2 (already checked), 1/5, 2/5
        twoPairsCount++; // saves card as two pairs  
      } // closes if same face value for cards 3/4 and 1/5, 2/5
      else if (((card4Pair == card5Pair) && (card1Pair == card3Pair))) { // if same face value for cards 4/5 and 1/2 (already checked), 2/3 (aready checked), 1/3
        twoPairsCount++; // saves card as two pairs   
      } // closes if same face value for cards 4/5 and 1/3
      else if (((card1Pair == card3Pair) && (card2Pair == card4Pair)) || ((card1Pair == card3Pair) && (card2Pair == card5Pair))) { // if same face value for cards 1/3 and 2/4, 4/5 (alrady checked), 2/5
        twoPairsCount++; // saves card as two pairs
      } // closes if same face value for cards 1/3 and 2/4, 2/5
      else if (((card1Pair == card4Pair) && (card2Pair == card5Pair)) || ((card1Pair == card4Pair) && (card3Pair == card5Pair))) { // if same face value for cards 1/4 and 2/3 (already checked), 2/5, 3/5 
        twoPairsCount++; // saves card as two pairs
      } // closes if same face value for cards 1/4 and 2/5, 3/5 
      else if ((card1Pair == card5Pair) && (card2Pair == card4Pair)) { // if same face value for cards 1/5 and 2/3 (already checked), 3/4 (already checked), 2/4
        twoPairsCount++; // saves card as two pairs
      } // closes if same face value for cards 1/5 and 2/4
      else if ((card2Pair == card4Pair) && (card3Pair == card5Pair)) { // if same face value for cards 2/4 and 1/3 (already checked), 1/5(already checked), 3/5
        twoPairsCount++; // saves card as two pairs
      } // closes if same face value for cards 2/4 and 3/5
      // all 2/5 matches aleady checked (1/3, 3/4, 1/4) 
      // all 3/5 matches already checked (1/2, 1/4, 2/4) 

      // checks if one pair 
      else if ((card1Pair == card2Pair) || (card1Pair == card3Pair) || (card1Pair == card4Pair) || (card1Pair == card5Pair)) { // if card 1 is pair with another card (same face value)
        onePairCount++; // saves card as one pair 
      } // closes if card 1 is pair with another card 
      else if ((card2Pair == card3Pair) || (card2Pair == card4Pair) || (card2Pair == card5Pair)) { // if card 2 is pair with another card (same face value)
        onePairCount++; // saves card as one pair 
      } // closes if card 2 is pair with another card 
      else if ((card3Pair == card4Pair) || (card3Pair == card5Pair)) { // if card 3 is pair with another card (same face value)
        onePairCount++; // saves card as one pair 
      } // closes if card 3 is pair with another card 
      else if (card4Pair == card5Pair) { // if card 4 is pair with another card (same face value)
        onePairCount++; // saves card as one pair 
      } // closes if card 4/5 are paired with another card 
          
      // incriment handsGenerated for next go through loop 
      handsGenerated++;    
    } // closes while (handsGenerated <= handsWanted)
    
    // calculate probablitiy of hands 
    // calculate probablity of four of a kind 
    double fourOfAKindProbability = ((double)fourOfAKindCount/(double)handsWanted); 
    // calculate probability of three of a kind 
    double threeOfAKindProbability = ((double)threeOfAKindCount/(double)handsWanted);
    // calculate probability of two pairs 
    double twoPairsProbability = ((double)twoPairsCount/(double)handsWanted);
    // calculate probability of one pair 
    double onePairProbability = ((double)onePairCount/(double)handsWanted);
    
    // print output 
    System.out.println("The number of loops: " + handsWanted);
    System.out.printf("The probability of four-of-a-kind: %1.3f", fourOfAKindProbability);
    System.out.println(" ");
    System.out.printf("The probability of three-of-a-kind: %1.3f", threeOfAKindProbability);
    System.out.println(" ");
    System.out.printf("The probability of two pairs: %1.3f", twoPairsProbability);
    System.out.println(" ");
    System.out.printf("The probability of one pair: %1.3f", onePairProbability);
    System.out.println(" ");
    
  } // closes main method
} // closes class