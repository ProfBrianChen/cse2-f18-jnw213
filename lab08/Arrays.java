/////////////////
// Jannah Wing
// CSE 002- 010 
// Lab 08: Arrays 
//
// Creates two arrays 
//      Array one with random integers 
//      Array two giving the occurance of each integer in the first array

public class Arrays{
  public static void main (String[] args) {
    
    // declare arays as varaibles, create arrays, and specify size 
    int[] array1 = new int[100]; 
    int[] array2 = new int[100];
    
    int myRandomNumber = -1; // intitialize 
    
    System.out.print("Array 1 holds: ");
    for (int i=0; i < 100; i++) {
      array1[i] = (int) (Math.random()*(99 - 0 + 1)); // gets number between 0 - 99 
      System.out.print(array1[i] + ", ");
    }
    System.out.println();
    
    for (int k=0; k <100; k++) {
      // intiitalize varaibales
      int counter = 0; 
      int innerloop = 0;
      while (innerloop < 100) {
        if (array1[innerloop] == k) {
          counter++; // increments counter 
        } // increments if array1 int is equal to k (position in list of array2)
        innerloop++; 
      }
      array2[k] = counter; // assignes aray 2 position as number of times that number in array 1 
      if (array2[k] > 0) {
        System.out.println(k + " occurs " + array2[k] + " time(s)");
      }
    }
    
  } // closes class
} // closes main method 
