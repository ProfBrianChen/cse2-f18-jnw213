/////////////////
// Jannah Wing
// CSE 002- 010 
// HW 08: Arrays 
//
// 1. Gives deck of 52 cards
// 2. Prints all cards in deck
// 3. Shuffles deck
// 4. Prints out shuffled cards
// 5. Gets a hand of cards & prints 

import java.util.Scanner;

public class Arrays{
  public static void main (String[] args) { // taken from CSE 002 HW description & edited 
    Scanner scan = new Scanner(System.in); 
    // Define string for suit names (clubs, hearts, spades, diamonds)
    String[] suitNames={"C","H","S","D"};    
    // Define string for card ranks (1-10, jack, queen, king, ace)
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    // Declare new string wih 52 elements for 52 cards in a deck
    String[] cards = new String[52]; 
    
    // Loop to define & print elements in deck of cards (String[] cards) 
    for (int i = 0; i < 52; i++){ 
      cards[i] = rankNames[i%13] + suitNames[i/13]; 
    } // closes for 
    
    // Prints original hand
    System.out.println("Original Hand: ");
    printArray(cards);
    
    // Shuffles and prints shuffled hand
    System.out.println("Shuffled Hand: ");
    cards = shuffle(cards); 
    printArray(cards); 
    
    // Get x numCards 
    System.out.print("How many cards per hand (max 104)? "); 
    int numCards = scan.nextInt(); 
    // Declare new string with 5 elements for 5 cards in a hand 
    String[] hand = new String[numCards]; 
    int again = 1; 
    int index = 51;
    while(again == 1){ 
      System.out.println("Hand: ");
      if ((index-numCards) < 0) { // if not enough cards left in deck; creates a new deck to draw from & restarts index at 51
        cards = shuffle(cards); 
        index = 51; 
      }
      hand = getHand(cards,index,numCards); 
      printArray(hand);
      index = index - numCards; 
      System.out.println("Enter a 1 if you want another hand drawn"); 
      boolean againCheck = false;
      while (!againCheck) { // while incorrect input (again is not integer)
        // check that user input in correct form
        againCheck = scan.hasNextInt(); // will return true if user types int and false if not 
        if (againCheck){ // if correct form
          again = scan.nextInt(); 
        } // closes if
        else if (!againCheck) { // if incorrect form
          scan.next(); // discards incorrect input 
          again = 0; // if enter anything but 1; again is 0, making outer while false and ending program
        } // closes else if
      } // closes inner while 
    } // closes outer while
    
  } // closes main method 
  
  public static void printArray(String[] list) {
    for (int i = 0; i < list.length; i++){ 
      System.out.print(list[i]+" ");
    } // closes for 
    System.out.println();
  } // closes printArray method

  public static String[] shuffle(String[] list) {
    // create placeholder array
    String[] placeholder = new String[1];
    // loop through shuffle 100 times 
    for (int i = 0; i < 100; i++){
      // Get randoms 
      int random = (int)(Math.random()*(51 - 1 + 1)); // gets random number between 1-51 
      // Switch cards 
      placeholder[0] = list[0]; 
      list[0] = list[random];
      list[random] = placeholder[0]; 
    } // closes for 
    return list; 
  } // closes shuffle method
  
  public static String[] getHand(String[] list, int index, int numCards) {
    String[] hand = new String[numCards]; 
    int handIndex = 0; 
    
    // if numCards is greater than 52, or reaching the end of decks (index-numCard is negative), make more decks 
    boolean needMoreDecks = false; 
    String[] cards2 = new String[52]; // intitializes second deck incase needed 
    if (numCards > 52) {
      needMoreDecks = true; 
    }
    if (needMoreDecks) {
      cards2 = shuffle(list); 
    }
    
    // loop to define each element in hand 
    for (int i = index; i > (index - numCards); i--) {
      if (!needMoreDecks) { // if only one deck needed to define all elements in hand
        hand[handIndex] = list[i]; 
      } // closes if 
      else { // if two decks needed to define all elements in hand 
        if (i >= 0) {
          hand[handIndex] = list[i]; 
        } // closes inner if 
        else {
          hand[handIndex] = cards2[52 + i];
        } // closes inner else 
      } // closes else 
      handIndex++; // increments handIndex
    } // closes for loop 
    return hand; 
  } // closes getHands method
  
} // closes public class 