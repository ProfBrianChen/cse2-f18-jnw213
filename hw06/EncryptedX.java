////////////////////
// Jannah Wing
// CSE 002
// HW #6
// Due October 23, 2018
// 
// Prints hidden x within grid of *'s 
//
// Input variables: given by user
//     Integer between 0-100 to specify grid length/width
//
// Output variables: printed pattern 
//     with number of rows/columns based on input 

// import Scanner class 
import java.util.Scanner; 
// define a class
public class EncryptedX {
  // add main method required for every Java program
  public static void main(String [] args) {
    
    // declare an instance of the Scanner object 
    Scanner myScanner = new Scanner (System.in);
    
    // get input 
    
    // printed statement asking user to input an integer between 0-100
    System.out.print("Enter an integer side length between 0-100: ");
    
    // check that input is integer in range, and if not, let user reenter correctly 
    // initialize boolean and integer used in loop 
    boolean integerCheck = false; 
    int integerGiven = -1;
    // use loop to let user input number, check that it is integer in range, and reenter if necessary  
    while (!integerCheck) { // while incorrect input (not integer 0-100)
      // check that user input in correct form
      integerCheck = myScanner.hasNextInt(); // will return true if user types int and false if not 
      if (integerCheck){ // if correct form
        int integerTemporary = myScanner.nextInt(); 
        if ((integerTemporary >= 0) && (integerTemporary <= 100)) { // if correct range
          integerGiven = integerTemporary; // define input as integerGiven
        } // closes if 
        else { // if not in range
          integerCheck = false; // define as false so reenter loop
          System.out.println("Incorrect Range, please try again.");
          System.out.print("Enter an integer between 0-100: ");
        } // closes else 
      } // closes if
      else if (!integerCheck) { // if incorrect form
        myScanner.next(); // discards incorrect input 
        System.out.println("Incorrect Input, please try again.");
        System.out.print("Enter an integer between 0-100: ");
      } // closes else if
    } // closes while
    
    // print pattern
    
    // add one to integerGiven to account for embedded X
    integerGiven++; 
    // initialize variables 
    int numRow = 0;
    int numCol = 0; 
    // nested loop to print pattern
    for (numRow = 1; numRow <= integerGiven; numRow++) { // prints integerGiven number of rows  
      for (numCol = 1; numCol <= integerGiven; numCol++) { // prints integerGiven number of columns 
        if (numCol == numRow) { // prints space for \ diagonal 
          System.out.print(" ");
        } // closes if 
        else if (numCol == (integerGiven - numRow + 1)){ // prints space for / diagonal
          System.out.print(" ");
        } // closes else if 
        else { // prints * for all other row/column combos 
          System.out.print("*");
        } // closes else
      } // closes inner for loop
      System.out.println(); // go to next row 
    } // closes outer for loop
  } // closes main method
} // closes public class 