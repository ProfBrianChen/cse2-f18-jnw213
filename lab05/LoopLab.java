////////////////////
// Jannah Wing
// CSE 002
// Lab #5: Loop Lab
// Due September 12, 2018
// 
// Uses infite loops to check that user is inputing data in the correct form 
//
// Input variables: given by user
//     Course number
//     Department name 
//     Number of time class meets per week
//     Instructor name 
//     Number of students 
//
// Output variables: printed comments on each input 
//     If incorrect, Error message telling user that they input incorrect type, and to try again
//     If correct, tells correct input

// import Scanner class 
import java.util.Scanner; 
// define a class
public class LoopLab {
  // add main method required for every Java program
  public static void main(String [] args) {
    // declare an instance of the Scanner object 
    Scanner myScanner = new Scanner (System.in);
    
    // print statement asking user to input course number
    System.out.print("Enter course number (integer component only): ");
    // initialize boolean to enter loop
    boolean courseNumberCheck = false; 
    // use loop to let user input answer, check that it is correct, and reenter if necessary  
    while (!courseNumberCheck) { // while incorrect input (course number not integer)
      // check that user input in correct form
      courseNumberCheck = myScanner.hasNextInt(); // will return true if user types int and false if not 
      if (courseNumberCheck){ // if correct form
        int courseNumber = myScanner.nextInt(); 
        System.out.println("Good Course Number input"); // check REMOVE
      } // closes if
      else if (!courseNumberCheck) { // if incorrect form
        myScanner.next(); // discards incorrect input 
        System.out.println("Incorrect Input, please try again.");
        System.out.print("Enter course number (integer component only): ");
      } // closes else 
    } // closes while
      
    // print statement asking user to input department name 
    System.out.print("Enter department name: "); 
    // initialize boolean to enter loop
    boolean departmentNameCheck = true; 
    // use loop to let user input answer, check that it is correct, and reenter if necessary  
    while (departmentNameCheck) { // while department name is integer 
      // check that user input in correct form
      departmentNameCheck = myScanner.hasNextInt(); // will return true if user types int and false if not 
      if (!departmentNameCheck){ // if correct form
        // allow user to input department name (full line without whitespace) 
        String departmentName = myScanner.next();  
        System.out.println("Good Department name input"); // check REMOVE
      } // closes if
      else if (departmentNameCheck) { // if incorrect form
        myScanner.next(); // discards incorrect input 
        System.out.println("Incorrect Input, please try again.");
        System.out.print("Enter department name: ");
      } // closes else 
    } // closes while 
    
    //print statement asking user to input the number of times the class meets per week
    System.out.print("Enter the number of times the class meets per week (as integer): ");
    // initialize boolean to enter loop
    boolean meetingsWeeklyCheck = false; // while meetingsweekly is not integer
    // use loop to let user input answer, check that it is correct, and reenter if necessary  
    while (!meetingsWeeklyCheck) {
      // check that user input in correct form
      meetingsWeeklyCheck = myScanner.hasNextInt(); // will return true if user types int and false if not
      if (meetingsWeeklyCheck){ // if correct form
        // allow user to input the number of times the class meets per week
        int meetingsWeekly = myScanner.nextInt();
        System.out.println("Good number of meetings input"); // check REMOVE 
      } // closes if
      else if (!meetingsWeeklyCheck) { // if incorrect form
        myScanner.next(); // discards incorrect input 
        System.out.println("Incorrect Input, please try again.");
        System.out.print("Enter the number of times the class meets per week (as integer): ");
      } // closes else 
    } // closes while
       
    //print statement asking user to input the time the class starts 
    System.out.print("Enter the time the class starts (as integer in military time (ex. 0930)): ");
    // initialize boolean to enter loop
    boolean timeStartCheck = false; // while start time is not integer
    // use loop to let user input answer, check that it is correct, and reenter if necessary  
    while (!timeStartCheck) {
      // check that user input in correct form
     timeStartCheck = myScanner.hasNextInt(); // will return true if user types int and false if not
      if (timeStartCheck){ // if correct form
        // allow user to input the time the class starts (hours)
        int timeStarts = myScanner.nextInt();
        System.out.println("Good start time input"); // check REMOVE 
      } // closes if
      else if (!timeStartCheck) { // if incorrect form
        myScanner.next(); // discards incorrect input 
        System.out.println("Incorrect Input, please try again.");
        System.out.print("Enter the time the class starts (as integer in military time (ex. 0930)): ");
      } // closes else 
    } // closes while
        
    //print statement asking user to input the instructor name
    System.out.print("Enter the instructor name (no spaces, last name only): ");
    // initialize boolean to enter loop
    boolean instructorNameCheck = true; 
    // use loop to let user input answer, check that it is correct, and reenter if necessary  
    while (instructorNameCheck) { // while instructor name is integer 
      // check that user input in correct form
      instructorNameCheck = myScanner.hasNextInt(); // will return true if user types int and false if not 
      if (!instructorNameCheck){ // if correct form (not integer)
        // allow user to input instructor name (full line without whitespace) 
        String instructorName = myScanner.next();  
        System.out.println("Good instructor name input"); // check REMOVE
      } // closes if
      else if (instructorNameCheck) { // if incorrect form
        myScanner.next(); // discards incorrect input 
        System.out.println("Incorrect Input, please try again.");
        System.out.print("Enter the instructor name (no spaces, last name only): ");
      } // closes else 
    } // closes while
    
    //print statement asking user to input the number of students
    System.out.print("Enter the number of students in the class (as integer): ");
    // initialize boolean to enter loop
    boolean numberStudentsCheck = false; 
    // use loop to let user input answer, check that it is correct, and reenter if necessary  
    while (!numberStudentsCheck) { // while number students is not integer
      // check that user input in correct form
      numberStudentsCheck = myScanner.hasNextInt(); // will return true if user types int and false if not
      if (numberStudentsCheck){ // if correct form
        // allow user to input the number students in the class 
        int numberStudents = myScanner.nextInt();
        System.out.println("Good number students input"); // check REMOVE 
      } // closes if
      else if (!numberStudentsCheck) { // if incorrect form
        myScanner.next(); // discards incorrect input 
        System.out.println("Incorrect Input, please try again.");
        System.out.print("Enter the number of students in the class (as integer): ");
      } // closes else 
    } // closes while
    
  } // closes main method
} // closes class 