////////////////////
// Jannah Wing
// CSE 002
// Lab #6: PatternC
// Due October 19, 2018
// 
// Prints Pattern C

// import Scanner class 
import java.util.Scanner; 
// define a class
public class PatternC {
  // add main method required for every Java program
  public static void main(String [] args) {
    // declare an instance of the Scanner object 
    Scanner myScanner = new Scanner (System.in);
    
    // print statement asking user to input course number
    System.out.print("Enter Integer between 1 and 10: ");
    // initialize variables used outside the loop
    int intEntered = 0;
    // initialize boolean to enter loop
    boolean intCheck = false; 
    // use loop to let user input answer, check that it is correct integer, and reenter if necessary  
    while (!intCheck) { // while incorrect input (not integer)
      // check that user input in correct form
      intCheck = myScanner.hasNextInt(); // will return true if user types int and false if not 
      if (intCheck){ // if correct form
        int intTemporary = myScanner.nextInt(); 
        if ((intTemporary >=1) && (intTemporary <= 10)) {
          intEntered = intTemporary;
          System.out.println("Good integer input. Your integer is: " + intEntered); // check REMOVE
        } // closes if 
        else {
          intCheck = false; // sets intCheck as false so can reenter loop even though integer
          System.out.println("Incorrect range. Please try again."); 
          System.out.print("Enter Integer between 1 and 10: ");
        } // closes else
      } // closes if (intCheck) 
      else if (!intCheck) { // if incorrect form
        myScanner.next(); // discards incorrect input 
        System.out.println("Incorrect format, please try again.");
        System.out.print("Enter Integer between 1 and 10: ");
      } // closes else if 
    } // closes while 
    
    // nested loop to make pattern 
    for (int numRow = 1; numRow <= intEntered; numRow++) { // prints new row as long as k <= int entered 
      int print = 0; 
      for (int numCol = 1; numCol <= intEntered; numCol++) { // prints number on same line until = row # 
        if (numCol < (intEntered - numRow + 1)) {
          System.out.print(" ");
        }
        else {
          int printC = intEntered - print;
          System.out.print(printC); 
        }
        print++; // increments print value 
      }
      System.out.println();
    }
  } // closes main method
} // closes class 
    