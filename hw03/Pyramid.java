////////////////////
// Jannah Wing
// CSE 002
// HW #3: Pyramid
// Due September 18, 2018
// 
// Prints information about the volume inside square-base pyramid
//
// Input variables: obtained from user via Scanner class 
//     Length of square side of pyrmaid 
//     Height of pyramid 
//
// Output variables: printed statements including
//     Volume of pyramid

// import Scanner class
import java.util.Scanner;
// define a class
public class Pyramid {
  // add main method required for every Java program
  public static void main(String [] args) {
    // declare an instance of the Scanner object & call the Scanner constructor
    Scanner myScanner = new Scanner (System.in); // prepares so ready for input
    
    // declare and get input 
    
    // print statement asking user to input the length of the square side of the pyramid
    System.out.print("Enter the length of the square side of the pyramid: ");
    // allow user to input length
    double lengthSquare = myScanner.nextDouble(); 
    
    // print statment asking user to input height of the pyramid 
    System.out.print("Enter the height of the pyramid: ");
    // allow user to input height
    double heightPyramid = myScanner.nextDouble();
    
    // calculate volume inside pyramid 
    
    // use equation V = (1/3) * base * height to calculate volume inside pyramid 
    int volumePyramid = (int)(lengthSquare * heightPyramid / 3);
    
    // print output
    
    // prints statement telling the volume inside the pyramid 
    System.out.print("The volume inside the pyramid is: " + volumePyramid);
    
  } // closes main method
  
} // closes class 