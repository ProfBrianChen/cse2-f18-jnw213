////////////////////
// Jannah Wing
// CSE 002
// HW #3: Convert
// Due September 18, 2018
// 
// Prints information about amount of water dropped on land by hurricane
//
// Input variables: obtained from user via Scanner class 
//     Number of acres of land affected by hurricane 
//     Average inches of rain dropped
//
// Output variables: printed statements including
//     Quantity of rain dropped in cubic miles

// import Scanner class
import java.util.Scanner;
// define a class
public class Convert {
  // add main method required for every Java program
  public static void main(String [] args) {
    // declare an instance of the Scanner object & call the Scanner constructor
    Scanner myScanner = new Scanner (System.in); // prepares so ready for input
    
    // declare and get input 
    
    // print statement asking user to input number of acres of land affected by hurricane
    System.out.print("Enter the affected area in acres (with decimal): ");
    // allow user to input number of acres  
    double numberAcres = myScanner.nextDouble(); 
    
    // print statement asking user to input average inches of rain dropped
    System.out.print("Enter the average inches of rainfall in the afffected area: ");
    // allow user to input inches of rain 
    double inchesRain = myScanner.nextDouble();
    
    // calculate cubic miles
    
    // convert to gallons (1 in of rain on 1 acre of ground is equal to about 27,154 gallons)
    double gallonsRain = 27154 * inchesRain * numberAcres; 
    // convert to cubic miles (1 gallon is equal to about 9.08169e-13 cubic miles)
    double cubicMilesRain = (9.08169 * (Math.pow(10, -13))) * gallonsRain;
    
    // print output
    
    // print statement telling user the quantity of rain dropped in cubic miles
    System.out.print("The amount of rain dropped is " + cubicMilesRain + " cubic miles.");
      
  } // closes main method
  
} // closes class 
