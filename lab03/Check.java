////////////////////
// Jannah Wing
// CSE 002
// Lab #3: Check
// Due September 21, 2018
// 
// Prints information regarding how to split a check among a group
// of friends who went out for dinner
//
// Input variables: obtained from user via Scanner class 
//     Original cost of check
//     Percentage tip they wish to pay
//     Number of ways the check will be split
//
// Output variables: printed statements including
//     How much each person in the group needs to pay

// import Scanner class
import java.util.Scanner;
// define a class
public class Check {
  // add main method required for every Java program
  public static void main(String [] args) {
    // declare an instance of the Scanner object & call the Scanner constructor
    Scanner myScanner = new Scanner (System.in); // prepares so ready for input
    
    // declare and get input 
    
    // print statement asking user for cost of check 
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    // allow user to input cost of check 
    double checkCost = myScanner.nextDouble();
    
    // print statement asking user for the tip percentage they would like to pay
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):  ");
    // allow user to input tip percentage they would like to use
    double tipPercent = myScanner.nextDouble();
    // convert the tip percentage into a decimal value 
    tipPercent /= 100; 
    
    // print statement asking user for the number of people paying
    System.out.print("Enter the number of people who went out to dinner: ");
    // allow user to input number of people paying
    int numPeople = myScanner.nextInt();
    
    // prepare & give output
    
    // declare variables
    double totalCost; // total cost of check including desired tip
    double costPerPerson; // totcal cost of check per person
    int dollars, // whole dollar mount of cost 
    dimes, pennies; // for storing digits to the right of decimal point
    
    // calcuate output 
    totalCost = checkCost * (1 + tipPercent); // calculates total cost of bill including tip
    costPerPerson = totalCost / numPeople; // calculates total cost per person
    
    // get output into $xx.xx form 
    dollars = (int) costPerPerson; // gets whole amount, dropping decimal fraction
    dimes = (int) (costPerPerson * 10) % 10; // gets .x place amount by returning remainder (%)
    pennies = (int) (costPerPerson * 100) % 10; // gets .0x place amount by returning remainder (%)
    
    // print amount each person needs to pay
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
    
  } // end of main method
} // end of class