////////////////////
// Jannah Wing
// CSE 002
// HW #4: CrapsSwitch
// Due September 25, 2018
// 
// Tells the user the slang score of a Craps roll (two six-sided dice) using switch statments 
//
// Input variables: obtained either from 
//     Random variable 
//     User input via scanner 
//
// Output variables: printed statements including
//     Slang score of dice roll

// import Scanner class
import java.util.Scanner;
// define a class
public class CrapsSwitch {
  // add main method 
  public static void main(String [] args) {
    // declare an instance of the Scanner object 
    Scanner myScanner = new Scanner (System.in);
    
    // print statement asking user if they would like to randomly cast or choose dice
    System.out.print("Would you like to randomly cast dice or choose dice numbers (enter 1 for cast or 2 for choose)?: ");
    // allow user to input choice 
    int choice = myScanner.nextInt(); 
    
    switch (choice){ // switches between user chosing cast, choice, or neither
      case 1: // if random cast
        // get random integer in range [0,6) (0 to 5 including 5) to represent first dice roll 
        int dice1 = (int) (Math.random()*6);  
        // get random integer in range [0,6) (0 to 5 including 5) to represent second dice roll 
        int dice2 = (int) (Math.random()*6);
        
        // determine slang terminology of roll & print statement based on value of dice 1 and 2
        switch (dice1) {
          case 1: // if dice 1 = 1
            switch (dice2) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled a Snake Eyes");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled an Ace Deuce");
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled an Easy Four");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled a Fever Five");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled an Easy Six");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled a Seven Out");
                break;
            } // close switch (dice2)
            break; // breaks case 1
          
          case 2: // if dice 1 = 2
            switch (dice2) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled an Ace Deuce");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled a Hard Four");
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled a Fever Five");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled an Easy Six");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled a Seven Out");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled an Easy Eight");
                break;
            } // closes switch (dice2)
            break; // breaks case 2
            
          case 3: // if dice 1 = 3
            switch (dice2) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled an Easy Four");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled an Fever Five");
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled a Hard Six");
                break;
              case 4: // if dice 2 = 4 
                System.out.println("You rolled an Seven Out");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled an Easy Eight");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled a Nine");
                break;   
            } // closes switch (dice2)
            break; // breaks case 3
            
          case 4: // if dice 1 = 4
            switch (dice2) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled a Fever Five");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled an Easy Six");
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled an Seven Out");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled a Hard Eight");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled a Nine");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled an Easy Ten");
                break;
            } // closes switch (dice2)
            break; // breaks case 4
            
          case 5: // if dice 1 = 5
            switch (dice2) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled an Easy Six");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled a Seven Out"); 
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled an Easy Eight");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled a Nine");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled a Hard Ten");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled a Yo-leven");
                break;               
            } // closes switch (dice2)
            break; // breaks case 5
            
          case 6: // if dice 1 = 1
            switch (dice2) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled a Seven Out");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled an Easy Eight");
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled a Nine");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled a Easy Ten");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled a Yo-leven");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled a Boxcars");
                break;
            } // closes switch (dice2)
            break; // breaks case 6
            
        } // closes switch(dice1)
        break; // breaks choice case 1
     
      case 2: // if choose dice numbers
        // ask the user for dice value of first role
        System.out.println("What is the value of your first dice roll?: ");
        // allow user to input choice
        int dice1b = myScanner.nextInt();
        // ask the user for dice value of second role
        System.out.println("What is the value of your second dice roll?: ");
        // allow user to input choice
        int dice2b = myScanner.nextInt();
      
        // determine slang terminology of roll & prints statement based on value of dice 1 and 2
        switch (dice1b) { 
          case 1: // if dice 1 = 1
            switch (dice2b) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled a Snake Eyes");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled an Ace Deuce");
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled an Easy Four");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled a Fever Five");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled an Easy Six");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled a Seven Out");
                break;
              default: // if dice 2 does not equal integer 1 to 6
                System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
                break;    
            } // close switch (dice2)
            break; // breaks case 1
          
          case 2: // if dice 1 = 2
            switch (dice2b) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled an Ace Deuce");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled a Hard Four");
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled a Fever Five");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled an Easy Six");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled a Seven Out");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled an Easy Eight");
                break;
              default: // if dice 2 does not equal integer 1 to 6
                System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
                break;
            } // closes switch (dice2)
            break; // breaks case 2
            
          case 3: // if dice 1 = 3
            switch (dice2b) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled an Easy Four");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled an Fever Five");
                break;
              case 3: // if dice 2 = 3 
                System.out.println("You rolled a Hard Six");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled an Seven Out");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled an Easy Eight");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled a Nine");
                break;
              default: // if dice 2 does not equal integer 1 to 6
                System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
                break;   
            } // closes switch (dice2)
            break; // breaks case 3
            
          case 4: // if dice 1 = 4
            switch (dice2b) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled a Fever Five");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled an Easy Six");
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled an Seven Out");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled a Hard Eight");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled a Nine");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled an Easy Ten");
                break;
              default: // // if dice 2 does not equal integer between 1 and 6
                System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
                break;
            } // closes switch (dice2)
            break; // breaks case 4
            
          case 5: // if dice 1 = 5
            switch (dice2b) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled an Easy Six");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled a Seven Out"); 
                break;
              case 3: // if dice 2 = 3
                System.out.println("You rolled an Easy Eight");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled a Nine");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled a Hard Ten");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled a Yo-leven");
                break;
              default: // if dice 2 does not equal integer between 1 and 6
                System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
                break;               
            } // closes switch (dice2)
            break; // breaks case 5
            
          case 6: // if dice 1 = 6
            switch (dice2b) {
              case 1: // if dice 2 = 1
                System.out.println("You rolled a Seven Out");
                break;
              case 2: // if dice 2 = 2
                System.out.println("You rolled an Easy Eight");
                break;
              case 3: // if dice 2 = 2
                System.out.println("You rolled a Nine");
                break;
              case 4: // if dice 2 = 4
                System.out.println("You rolled a Easy Ten");
                break;
              case 5: // if dice 2 = 5
                System.out.println("You rolled a Yo-leven");
                break;
              case 6: // if dice 2 = 6
                System.out.println("You rolled a Boxcars");
                break;
              default: // if dice 2 does not equal integer between 1 and 6
                System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
                break;
            } // closes switch (dice2)
            break; // breaks case 6
            
          default:
            System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
            break; // breaks default

        } // closes switch(dice1b)
        break; // breaks choice case 2
        
      default: // if invalid choice (not 1 or 2 for cast or choose)
        System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
        break; // breaks choice default
    
    } // switch (choice) 
    
  } // closes main method
  
} // closes class 