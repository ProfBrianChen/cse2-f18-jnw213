////////////////////
// Jannah Wing
// CSE 002
// HW #4: CrapsIf
// Due September 25, 2018
// 
// Tells the user the slang score of a Craps roll (two six-sided dice) using if else statments 
//
// Input variables: obtained either from 
//     Random variable 
//     User input via scanner 
//
// Output variables: printed statements including
//     Slang score of dice roll

// import Scanner class
import java.util.Scanner;
// define a class
public class CrapsIf {
  // add main method 
  public static void main(String [] args) {
    // declare an instance of the Scanner object 
    Scanner myScanner = new Scanner (System.in);
    
    // print statement asking user if they would like to randomly cast or choose dice
    System.out.print("Would you like to randomly cast dice or choose dice numbers (enter 1 for cast or 2 for choose)?: ");
    // allow user to input choice 
    int choice = myScanner.nextInt(); 
    
    if (choice == 1){ // if random cast
      // get random integer in range [0,6) (0 to 5 including 5) to represent first dice roll 
      int dice1 = (int) (Math.random()*6);  
      // get random integer in range [0,6) (0 to 5 including 5) to represent second dice roll 
      int dice2 = (int) (Math.random()*6);
      
      // determine slang terminology of roll
      if (dice1 == 1) { // if dice 1 = 1; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled a Snake Eyes");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled an Ace Deuce");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled an Easy Four");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled a Fever Five");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled an Easy Six");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled a Seven Out");
        } // closes else if (dice2 == 6) 
      } // closes if (dice1 == 1)   
      
      else if (dice1 == 2) { // if dice 1 = 2; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled an Ace Deuce");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled a Hard Four");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled a Fever Five");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled an Easy Six");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled a Seven Out");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled an Easy Eight");
        } // closes else if (dice2 == 6) 
      } // closes else if (dice1 == 2)
      
      else if (dice1 == 3) { // if dice 1 = 3; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled an Easy Four");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled an Fever Five");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled a Hard Six");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled an Seven Out");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled an Easy Eight");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled a Nine");
        } // closes else if (dice2 == 6) 
      } // closes else if (dice1 == 3) 
      
      else if (dice1 == 4) { // if dice 1 = 4; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled a Fever Five");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled an Easy Six");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled an Seven Out");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled a Hard Eight");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled a Nine");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled an Easy Ten");
        } // closes else if (dice2 == 6) 
      } // closes else if (dice1 == 4) 
      
      else if (dice1 == 5) { // if dice 1 = 5; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled an Easy Six");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled a Seven Out"); 
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled an Easy Eight");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled a Nine");
        } // closes else if (dice2 == 4)
        else if (dice2 == 5){
          System.out.println("You rolled a Hard Ten");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled a Yo-leven");
        } // closes else if (dice2 == 6) 
      } // closes else if (dice1 == 5) 
      
      else if (dice1 == 6) { // if dice 1 = 6; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled a Seven Out");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled an Easy Eight");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled a Nine");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled a Easy Ten");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled a Yo-leven");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled a Boxcars");
        } // closes else if (dice2 == 6) 
      } // closes else if (dice1 == 6)
      
    } // closes if (choice == 1) statement
    
    else if (choice == 2){ // if choose dice
      // ask the user for dice value of first role
      System.out.println("What is the value of your first dice roll?: ");
      // allow user to input choice
      int dice1 = myScanner.nextInt();
      // ask the user for dice value of second role
      System.out.println("What is the value of your second dice roll?: ");
      // allow user to input choice
      int dice2 = myScanner.nextInt();
      // prints invalid statment if invalid input
      
      // determine slang terminology of roll
      if (dice1 == 1) { // if dice 1 = 1; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled a Snake Eyes");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled an Ace Deuce");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled an Easy Four");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled a Fever Five");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled an Easy Six");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled a Seven Out");
        } // closes else if (dice2 == 6) 
        else {
          System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
        } // closes else 
      } // closes if (dice1 == 1)   
      
      else if (dice1 == 2) { // if dice 1 = 2; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled an Ace Deuce");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled a Hard Four");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled a Fever Five");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled an Easy Six");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled a Seven Out");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled an Easy Eight");
        } // closes else if (dice2 == 6) 
        else {
          System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
        } // closes else 
      } // closes else if (dice1 == 2)
      
      else if (dice1 == 3) { // if dice 1 = 3; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled an Easy Four");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled an Fever Five");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled a Hard Six");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled an Seven Out");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled an Easy Eight");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled a Nine");
        } // closes else if (dice2 == 6) 
        else {
          System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
        } // closes else
      } // closes else if (dice1 == 3) 
      
      else if (dice1 == 4) { // if dice 1 = 4; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled a Fever Five");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled an Easy Six");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled an Seven Out");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled a Hard Eight");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled a Nine");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled an Easy Ten");
        } // closes else if (dice2 == 6) 
        else {
          System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
        } // closes else
      } // closes else if (dice1 == 4) 
      
      else if (dice1 == 5) { // if dice 1 = 5; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled an Easy Six");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled a Seven Out"); 
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled an Easy Eight");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled a Nine");
        } // closes else if (dice2 == 4)
        else if (dice2 == 5){
          System.out.println("You rolled a Hard Ten");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled a Yo-leven");
        } // closes else if (dice2 == 6) 
        else {
          System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
        } // closes else
      } // closes else if (dice1 == 5) 
      
      else if (dice1 == 6) { // if dice 1 = 6; prints statement based on dice 2 value
        if (dice2 == 1) {
          System.out.println("You rolled a Seven Out");
        } // closes if (dice2 == 1) 
        else if (dice2 == 2){
          System.out.println("You rolled an Easy Eight");
        } // closes else if (dice2 == 2) 
        else if (dice2 == 3){
          System.out.println("You rolled a Nine");
        } // closes else if (dice2 == 3) 
        else if (dice2 == 4){
          System.out.println("You rolled a Easy Ten");
        } // closes else if (dice2 == 4) 
        else if (dice2 == 5){
          System.out.println("You rolled a Yo-leven");
        } // closes else if (dice2 == 5) 
        else if (dice2 == 6){
          System.out.println("You rolled a Boxcars");
        } // closes else if (dice2 == 6) 
        else {
          System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
        } // closes else
      } // closes else if (dice1 == 6)
      
      else { // if invalid entry ( dice 1 not between 1 and 6)
        System.out.println("Invalid entry. Please try again with integer between 1 and 6.");
      } // closes else (if dice1 is not between 1 and 6)
      
    } // closes if (choice = 2) statement 
    
    else { // if invalid entry (not 1 or 2 for cast or choose)
      System.out.println("Invalid entry. Please try again.");
      
    } // closes else 
    
  } // closes main method
  
} // closes class 