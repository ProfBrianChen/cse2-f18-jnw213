////////////////////
// Jannah Wing
// CSE 002
// Lab #4: Card Generator
// Due September 28, 2018
// 
// Outputs random playing card
//
// Input variables: random number generated automatically by program
//
// Output variables: printed statements including
//     Card suit: Spade, Diamond, Hearts, or Clubs
//     Card number: Ace, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, or King

// define a class
public class CardGenerator {
  // add main method required for every Java program
  public static void main(String [] args) {

    // Card Suits 
    
    // get random integer in range [0,4) (0 to 3 including 3) to represent suits
    int cardSuitNumber = (int) (Math.random()*4);  
    // replace cardsuit number with correct name 
    String cardSuit = "Insert"; // define string variable to replace number
    // if statements to switch cardsuit number with name 
    if (cardSuitNumber == 1) {
      cardSuit = "Diamonds";
    } // replaces cardSuit = 1 with diamonds  
    if (cardSuitNumber == 2) {
      cardSuit = "Clubs";
    } // replaces cardSuit = 2 with clubs
    if (cardSuitNumber == 3) {
      cardSuit = "Hearts";
    } // replaces cardSuit = 3 with hearts
    if (cardSuitNumber == 0) {
      cardSuit = "Spades";
    } // replaces cardSuit = 0 with spades
    
    // Assign card Number & print output statement 

    // get random integer in range [2,14) (2 to 13 including 13) to represent card number
    int cardNumber = (int) (Math.random()*(12)) + 2; 
    // fix card number for face cards & print statemetn 
    if (cardNumber == 1) {
      System.out.print("You picked the Ace of " + cardSuit);
    } // if card number is 1, print "You picked the Ace of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 2) {
      System.out.print("You picked the 2 of " + cardSuit);
    } // if card number is 2, print "You picked the 2 of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 3) {
      System.out.print("You picked the 3 of " + cardSuit);
    } // if card number is 3, print "You picked the 3 of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 4) {
      System.out.print("You picked the 4 of " + cardSuit);
    } // if card number is 4, print "You picked the 4 of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 5) {
      System.out.print("You picked the 5 of " + cardSuit);
    } // if card number is 5, print "You picked the 5 of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 6) {
      System.out.print("You picked the 6 of " + cardSuit);
    } // if card number is 6, print "You picked the 6 of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 7) {
      System.out.print("You picked the 7 of " + cardSuit);
    } // if card number is 7, print "You picked the 7 of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 8) {
      System.out.print("You picked the 8 of " + cardSuit);
    } // if card number is 8, print "You picked the 8 of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 9) {
      System.out.print("You picked the 9 of " + cardSuit);
    } // if card number is 9, print "You picked the 9 of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 10) {
      System.out.print("You picked the 10 of " + cardSuit);
    } // if card number is 10, print "You picked the 10 of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 11) {
      System.out.print("You picked the Jack of " + cardSuit);
    } // if card number is 5, print "You picked the Jack of (Diamons, Clubs, Hearts or Spades)
    if (cardNumber == 12) {
      System.out.print("You picked the Queen of " + cardSuit);
    } // if card number is 6, print "You picked the Queen of (Diamons, Clubs, Hearts or Spades)  
    if (cardNumber == 13) {
      System.out.print("You picked the King of " + cardSuit);
    } // if card number is 6, print "You picked the King of (Diamons, Clubs, Hearts or Spades)
    
  } // closes main method
  
} // closes class 