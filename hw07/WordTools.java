/////////////
// Jannah Wing
// CSE 002: HW07
// Due October 30, 2018
//
// Edits and identifies components in user texts
//
// Input: 
//      Text input by user 
// Output: 
//      Number of non-whitespace characters
//      Number of words
//      Number of occurances of specificed word
//      Text with ! replaced with . 
//      Text replaced with shortened spaces 

import java.util.Scanner; // imports scanner class 

public class WordTools { 
  
  public static void main (String [] args) { // main method
    // call scanner 
    Scanner myScanner = new Scanner(System.in);
    // Get text from sampleText method 
    String mySampleText = sampleText(); 
    System.out.println("You entered: " + mySampleText); 
    
    // Loop to call menu and run correct option
    boolean enterMenuLoop = true; 
    while (enterMenuLoop) { // keeps entering loop as long as dont quit 
      char menuOption = printMenu(); // calls printMenu method 
      System.out.println("You chose: " + menuOption); 
      
      // if want number of non-whitespace characters 
      if (menuOption == 'c') {
        int numOfNonWSCharacters = getNumOfNonWSCharacters(mySampleText); // calls numOfNonWSCharacters method
        System.out.println("The number of non-whitespace characters: " + numOfNonWSCharacters); 
      } // closes if c 
      
      // if want number of words
      else if (menuOption == 'w') {
        int numOfWords = getNumOfWords(mySampleText); // calls numOfWords method
        System.out.println("The number of words: " + numOfWords); 
      } // closes else if w
      
      // if want the number of occurances of word
      else if (menuOption == 'f') {
        System.out.print("Enter word you would like to count: "); 
        String textWantIdentify = myScanner.next(); // let user input word (until whitespace)
        int findTextOccurances = findText(textWantIdentify, mySampleText); // calls findText method
        System.out.println("The number of '" + textWantIdentify + "' instances: " + findTextOccurances);  
      } // closes else if f
      
      // if want to replace all ! 
      else if (menuOption == 'r') {
        String newReplacedString = replaceExclamation(mySampleText); // calls replaceExclamation method
        System.out.println("Edited text (without !): " + newReplacedString);
      } // closes else if r
      
      // if want to shorten spaces 
      else if (menuOption == 's') {
        String shortenedString = shortenSpace(mySampleText); // calls shortenSpace method
        System.out.println("Edited text (with shortened spaces): " + shortenedString); 
      } // closes else if s 
      
      // if want to quit menu/program
      else if (menuOption == 'q') {
        enterMenuLoop = false; // quits loop and ends program
      } // closes else if q
      
      // if incorrect input, give message and reenter loop 
      else {
        System.out.println("Incorrect input. Please try again."); 
      } // closes else 
    } // closes while enterMenuLoop   
  } // closes main method
  
  public static String sampleText () { // lets user input sample text
    Scanner myScanner = new Scanner(System.in); // calls instance of scanner
    System.out.println("Enter a sample text (any characters, no more than 3 continuous spaces): "); 
    String sampleTextMethod = myScanner.nextLine(); // lets user input text including whitespace 
    return sampleTextMethod; // returns string 
  } // closes sampleText method
  
  public static char printMenu () { // prints menu
    Scanner myScanner = new Scanner(System.in); // calls instance of scanner
    // print menu 
    System.out.println(" "); 
    System.out.println("MENU"); 
    System.out.println("c - Number of non-whitespace characters"); 
    System.out.println("w - Number of words");
    System.out.println("f - Find text"); 
    System.out.println("r - Replace all !'s"); 
    System.out.println("s - Shorten spaces"); 
    System.out.println("q - Quit"); 
    System.out.println(" "); 
    System.out.print("Choose an option: "); 
    // let user input single character (or take first character entered)
    char menuOptionMethod = myScanner.next().charAt(0); 
    return menuOptionMethod; // return character
  } // closes printMenu method 
  
  public static int getNumOfNonWSCharacters (String sampleTextMethod) { // gets number of non WS characters
    int numCharactersMethod = sampleTextMethod.length(); // number of characters
    int numWS = 0; // inititalizes numWS
    // loop through every character 
    for (int characterNumber = 0; characterNumber < numCharactersMethod; characterNumber++) {
      char characterAtNumber = sampleTextMethod.charAt(characterNumber); // gets char at position 
      // if character is WS, increment counter
      if (characterAtNumber == ' ') {
        numWS++; 
      } // closes if 
    } // closes for loop 
    // calculate num of non WS 
    int numOfNonWSCharactersMethod = numCharactersMethod - numWS; 
    return numOfNonWSCharactersMethod; // return int 
  } // closes getNumOfNonWSCharacters method
  
  public static int getNumOfWords (String sampleTextMethod) { // gets number of words
    int numCharactersMethod = sampleTextMethod.length(); // number of characters
    int numOfWordsMethod = 0; // inititalizes numOfWords
    // loop through every character in string 
    for (int characterNumber = 0; characterNumber < numCharactersMethod; characterNumber++) {
      char characterAtNumber = sampleTextMethod.charAt(characterNumber); // gets char at position 
      // if whitespace reached, end of word so increment
      if (characterAtNumber == ' ') {
        numOfWordsMethod++; 
        // check that not second space of double WS 
        int previousSpace = characterNumber - 1;
        char charAtPreviousSpace = sampleTextMethod.charAt(previousSpace); 
        if (charAtPreviousSpace == ' ') {
          numOfWordsMethod--; // if second WS of doubleWS, removes extra word count 
        } // closes if 
      } // closes if 
      
    } // closes for loop
    // check if last character is space 
    char lastChar = sampleTextMethod.charAt(numCharactersMethod-1); 
    boolean lastCharWS = Character.isWhitespace(lastChar); 
    if (!lastCharWS){ // if not white space
      numOfWordsMethod++; // increments numOfWords to account for final word in string without whitespace after
    }
    return numOfWordsMethod; // returns int
  } // closes getNumOfWords
  
  public static int findText (String textToFindMethod, String sampleTextMethodBefore) {
    // remove all double spaces and punctuation
    String sampleTextMethod = formatText(sampleTextMethodBefore); // calls methods to simplify text 
    
    // find number of characters 
    int numCharactersMethod = sampleTextMethod.length(); 
    
    // find number of words
    int numOfWordsMethod = 0; // inititalizes numOfWords
    for (int characterNumber = 0; characterNumber < numCharactersMethod; characterNumber++) {
      char characterAtNumber = sampleTextMethod.charAt(characterNumber); // gets char at position 
      // if whitespace reached, end of word so increment
      if (characterAtNumber == ' ') {
        numOfWordsMethod++; 
        // check that not second space of double WS 
        int previousSpace = characterNumber - 1;
        char charAtPreviousSpace = sampleTextMethod.charAt(previousSpace); 
        if (charAtPreviousSpace == ' ') {
          numOfWordsMethod--; // if second WS of doubleWS, removes extra word count 
        } // closes if 
      } // closes if 
    } // closes for loop
    // check if last character is space 
    char lastChar = sampleTextMethod.charAt(numCharactersMethod-1); 
    boolean lastCharWS = Character.isWhitespace(lastChar); 
    if (!lastCharWS){
      numOfWordsMethod++; // increments numOfWords to account for final word in string without whitespace after
    }
    
    // find index of whitespaces & redefine substring based on those indexes (gets each word in own string & checks)
    int charBegin = 0; // intitializes charBegin
    int charEnd = 0; // initializes charEnd
    int findText = 0; // initializes number times word in string
    for (int loopNumber = 1; loopNumber <= numOfWordsMethod; loopNumber++) {  
      if (loopNumber != 1) {
        charBegin = charEnd + 1; // if not first loop, charBegin is equal to white space at end of last loop + 1
      } // closes if 
      else {
        charBegin = 0; // if first loop, charBegin = 0
      } // closes else 
      // return location of whitespace after word
      int afterCharBegin = charBegin + 1; // gets number after charBegin to determine where to start charEnd
      if (loopNumber == numOfWordsMethod) {
        charEnd = numCharactersMethod; // if final loop, charEnd = last character
      } // closes if 
      else {
        charEnd = sampleTextMethod.indexOf(' ', afterCharBegin); // finds location of charEnd if not last word 
      } // closes else 
      // identify word within whitespaces 
      String wordInString = sampleTextMethod.substring(charBegin, charEnd); // identifies word based on character locations
      // check if word is desired word
      if (wordInString.equals(textToFindMethod)) {
        findText++; 
      } // closes if 
    } // closes for 
    return findText; 
  } // closes findText method 
  
  public static String replaceExclamation(String sampleTextMethod) {
    // find number of characters 
    int numCharactersMethod = sampleTextMethod.length(); 
    
    // find number of exclamations
    int numOfExclamations = 0; // inititalizes numOfWords
    for (int characterNumber = 0; characterNumber < numCharactersMethod; characterNumber++) { // loops through all characters
      char characterAtNumber = sampleTextMethod.charAt(characterNumber); // gets char at position 
      if (characterAtNumber == '!') {
        numOfExclamations++; // if !, incriments 
      } // closes if 
    } // closes for loop
    
    // find index of !, redefine substrings based on locations, create substrings of texts between !, repaste substrings with . in between
    int charBegin = 0; // intitializes charBegin
    int charEnd = 0; // initializes charEnd
    String stringPrevious = ""; // initializes StringPrevious
    String newString = ""; // initializes newString
    for (int loopNumber = 0; loopNumber <= numOfExclamations; loopNumber++) { // loop the number of times ! 
      if (loopNumber != 0) {
        charBegin = charEnd + 1; // if not first loop, charBegin is equal to position of ! at end of last loop + 1
      } // closes if 
      else {
        charBegin = 0; // if first loop, charBegin = 0
      } // closes else 
      // return location of ! after word
      int afterCharBegin = charBegin + 1; // gets number after charBegin to determine where to start charEnd
      if (loopNumber == numOfExclamations) {
        charEnd = numCharactersMethod; // if final loop, charEnd = last character
      } // closes if 
      else {
        charEnd = sampleTextMethod.indexOf('!', afterCharBegin); // finds location of charEnd if not last word 
      } // closes else 
      // identify string within !s
      String stringInString = sampleTextMethod.substring(charBegin, charEnd); // identifies word based on character locations
      // only add . to end of sentence if loopNumber < numOf Exclamations 
      if (loopNumber < numOfExclamations) {
        newString = stringPrevious + stringInString + "."; 
      } // closes if 
      else if (loopNumber == numOfExclamations) {
        newString = stringPrevious + stringInString; 
      } // closes else if 
      // add string to growing string 
      stringPrevious = newString; 
      } // closes for  
    return newString; // returns new string
    } // closes findExclamation method
  
  public static String shortenSpace(String sampleTextMethod) {
    // find number of characters 
    int numCharactersMethod = sampleTextMethod.length(); 
    
    // find number of double WS, redefine substrings of text between double WS, and paste back together with only one space between 
    int numOfDoubleWS = 0; // inititalizes numOfWords
    int charBegin = 0; // intitializes charBegin so first substring starts at beginning 
    int charEnd = 0; // initializes charEnd
    String stringPrevious = ""; // initializes StringPrevious
    String newString = ""; // initializes newString
    String stringInString = ""; // inititializes stringInString
    for (int characterNumber = 0; characterNumber < (numCharactersMethod - 1); characterNumber++) {
      char characterAtNumber1 = sampleTextMethod.charAt(characterNumber); // gets char at position 
      char characterAtNumber2 = sampleTextMethod.charAt(characterNumber + 1); // gets char at position after
      if ((characterAtNumber1 == ' ') && (characterAtNumber2 == ' ')) { // if two WS in a row 
        numOfDoubleWS++; // if Double WS, incriments 
        charEnd = characterNumber; // defines charEnd as first space of double WS 
        stringInString = sampleTextMethod.substring(charBegin, charEnd); // defines substring between double WS (or double Ws and beginning)
        charBegin = characterNumber + 2; // defines new charBegin as character after double whitespace 
        newString = stringPrevious + stringInString + " "; // adds substring to growing edited string 
        stringPrevious = newString; // saves string to be used in next loop
      } // closes if
    } // closes for loop 
    // add final substring after last double WS 
    charEnd = numCharactersMethod; // defines end of substring as end of text 
    stringInString = sampleTextMethod.substring(charBegin, charEnd); // defines substring
    newString = stringPrevious + stringInString; // adds substring to growing string 

    return newString; // return string 
  } // closes shortenSpace method 
  
  // extra methods to help findText
  
  public static String deleteCommas(String sampleTextMethod) {
    // Method deletes commas (COPY PASTED & edited from replaceExclamation)
    // find number of characters 
    int numCharactersMethod = sampleTextMethod.length(); 
    
    // find number of exclamations
    int numOfCommas = 0; // inititalizes numOfWords
    for (int characterNumber = 0; characterNumber < numCharactersMethod; characterNumber++) {
      char characterAtNumber = sampleTextMethod.charAt(characterNumber); // gets char at position 
      if (characterAtNumber == ',') {
        numOfCommas++; // if !, incriments 
      } // closes if 
    } // closes for loop
    
    // find index of !
    int charBegin = 0; // intitializes charBegin
    int charEnd = 0; // initializes charEnd
    String stringPrevious = ""; // initializes StringPrevious
    String newString = ""; // initializes newString
    for (int loopNumber = 0; loopNumber <= numOfCommas; loopNumber++) {  
      if (loopNumber != 0) {
        charBegin = charEnd + 1; // if not first loop, charBegin is equal to position of ! at end of last loop + 1
      } // closes if 
      else {
        charBegin = 0; // if first loop, charBegin = 0
      } // closes else 
      // return location of ! after word
      int afterCharBegin = charBegin + 1; // gets number after charBegin to determine where to start charEnd
      if (loopNumber == numOfCommas) {
        charEnd = numCharactersMethod; // if final loop, charEnd = last character
      } // closes if 
      else {
        charEnd = sampleTextMethod.indexOf(',', afterCharBegin); // finds location of charEnd if not last word 
      } // closes else 
      // identify string within !s
      String stringInString = sampleTextMethod.substring(charBegin, charEnd); // identifies word based on character locations
      // only add . to end of sentence if loopNumber < numOf Exclamations 
      if (loopNumber < numOfCommas) {
        newString = stringPrevious + stringInString + ""; 
      } // closes if 
      else if (loopNumber == numOfCommas) {
        newString = stringPrevious + stringInString; 
      } // closes else if 
      // add string to growing string 
      stringPrevious = newString; 
      } // closes for  
    return newString; // returns new string
    } // closes replacesCommas method
  
  public static String deleteSemis(String sampleTextMethod) {
    // Method deletes semicolons (COPY PASTED & edited from replaceExclamation)
    // find number of characters 
    int numCharactersMethod = sampleTextMethod.length(); 
    
    // find number of exclamations
    int numOfSemis = 0; // inititalizes numOfWords
    for (int characterNumber = 0; characterNumber < numCharactersMethod; characterNumber++) {
      char characterAtNumber = sampleTextMethod.charAt(characterNumber); // gets char at position 
      if (characterAtNumber == ';') {
        numOfSemis++; // if !, incriments 
      } // closes if 
    } // closes for loop
    
    // find index of !
    int charBegin = 0; // intitializes charBegin
    int charEnd = 0; // initializes charEnd
    String stringPrevious = ""; // initializes StringPrevious
    String newString = ""; // initializes newString
    for (int loopNumber = 0; loopNumber <= numOfSemis; loopNumber++) {  
      if (loopNumber != 0) {
        charBegin = charEnd + 1; // if not first loop, charBegin is equal to position of ! at end of last loop + 1
      } // closes if 
      else {
        charBegin = 0; // if first loop, charBegin = 0
      } // closes else 
      // return location of ! after word
      int afterCharBegin = charBegin + 1; // gets number after charBegin to determine where to start charEnd
      if (loopNumber == numOfSemis) {
        charEnd = numCharactersMethod; // if final loop, charEnd = last character
      } // closes if 
      else {
        charEnd = sampleTextMethod.indexOf(';', afterCharBegin); // finds location of charEnd if not last word 
      } // closes else 
      // identify string within !s
      String stringInString = sampleTextMethod.substring(charBegin, charEnd); // identifies word based on character locations
      // only add . to end of sentence if loopNumber < numOf Exclamations 
      if (loopNumber < numOfSemis) {
        newString = stringPrevious + stringInString + ""; 
      } // closes if 
      else if (loopNumber == numOfSemis) {
        newString = stringPrevious + stringInString; 
      } // closes else if 
      // add string to growing string 
      stringPrevious = newString; 
      } // closes for  
    return newString; // returns new string
    } // closes findExclamation method
  
  public static String deletePunctuation(String sampleTextMethod) { // replaces . with space
    // Method deletes periods (COPY PASTED & edited from replaceExclamation)
    // find number of characters 
    int numCharactersMethod = sampleTextMethod.length(); 
    
    // find number of exclamations
    int numOfPunctuation = 0; // inititalizes numOfWords
    for (int characterNumber = 0; characterNumber < numCharactersMethod; characterNumber++) {
      char characterAtNumber = sampleTextMethod.charAt(characterNumber); // gets char at position 
      if (characterAtNumber == '.') {
        numOfPunctuation++; // if !, incriments 
      } // closes if 
    } // closes for loop
    
    // find index of !
    int charBegin = 0; // intitializes charBegin
    int charEnd = 0; // initializes charEnd
    String stringPrevious = ""; // initializes StringPrevious
    String newString = ""; // initializes newString
    for (int loopNumber = 0; loopNumber <= numOfPunctuation; loopNumber++) {  
      if (loopNumber != 0) {
        charBegin = charEnd + 1; // if not first loop, charBegin is equal to position of ! at end of last loop + 1
      } // closes if 
      else {
        charBegin = 0; // if first loop, charBegin = 0
      } // closes else 
      // return location of ! after word
      int afterCharBegin = charBegin + 1; // gets number after charBegin to determine where to start charEnd
      if (loopNumber == numOfPunctuation) {
        charEnd = numCharactersMethod; // if final loop, charEnd = last character
      } // closes if 
      else {
        charEnd = sampleTextMethod.indexOf('.', afterCharBegin); // finds location of charEnd if not last word 
      } // closes else 
      // identify string within !s
      String stringInString = sampleTextMethod.substring(charBegin, charEnd); // identifies word based on character locations
      // only add . to end of sentence if loopNumber < numOf Exclamations 
      if (loopNumber < numOfPunctuation) {
        newString = stringPrevious + stringInString + ""; 
      } // closes if 
      else if (loopNumber == numOfPunctuation) {
        newString = stringPrevious + stringInString; 
      } // closes else if 
      // add string to growing string 
      stringPrevious = newString; 
      } // closes for  
    return newString; // returns new string
    } // closes findExclamation method
  
  public static String deleteExclamation(String sampleTextMethod) { // replaces . with space
    // Method deletes exclamations (COPY PASTED & edited from replaceExclamation)
    // find number of characters 
    int numCharactersMethod = sampleTextMethod.length(); 
    
    // find number of exclamations
    int numOfPunctuation = 0; // inititalizes numOfWords
    for (int characterNumber = 0; characterNumber < numCharactersMethod; characterNumber++) {
      char characterAtNumber = sampleTextMethod.charAt(characterNumber); // gets char at position 
      if (characterAtNumber == '!') {
        numOfPunctuation++; // if !, incriments 
      } // closes if 
    } // closes for loop
    
    // find index of !
    int charBegin = 0; // intitializes charBegin
    int charEnd = 0; // initializes charEnd
    String stringPrevious = ""; // initializes StringPrevious
    String newString = ""; // initializes newString
    for (int loopNumber = 0; loopNumber <= numOfPunctuation; loopNumber++) {  
      if (loopNumber != 0) {
        charBegin = charEnd + 1; // if not first loop, charBegin is equal to position of ! at end of last loop + 1
      } // closes if 
      else {
        charBegin = 0; // if first loop, charBegin = 0
      } // closes else 
      // return location of ! after word
      int afterCharBegin = charBegin + 1; // gets number after charBegin to determine where to start charEnd
      if (loopNumber == numOfPunctuation) {
        charEnd = numCharactersMethod; // if final loop, charEnd = last character
      } // closes if 
      else {
        charEnd = sampleTextMethod.indexOf('!', afterCharBegin); // finds location of charEnd if not last word 
      } // closes else 
      // identify string within !s
      String stringInString = sampleTextMethod.substring(charBegin, charEnd); // identifies word based on character locations
      // only add . to end of sentence if loopNumber < numOf Exclamations 
      if (loopNumber < numOfPunctuation) {
        newString = stringPrevious + stringInString + ""; 
      } // closes if 
      else if (loopNumber == numOfPunctuation) {
        newString = stringPrevious + stringInString; 
      } // closes else if 
      // add string to growing string 
      stringPrevious = newString; 
      } // closes for  
    return newString; // returns new string
    } // closes findExclamation method
  
  public static String deleteQuestionMarks(String sampleTextMethod) { // replaces . with space
    // Method deletes question marks (COPY PASTED & edited from replaceExclamation)
    // find number of characters 
    int numCharactersMethod = sampleTextMethod.length(); 
    
    // find number of exclamations
    int numOfPunctuation = 0; // inititalizes numOfWords
    for (int characterNumber = 0; characterNumber < numCharactersMethod; characterNumber++) {
      char characterAtNumber = sampleTextMethod.charAt(characterNumber); // gets char at position 
      if (characterAtNumber == '?') {
        numOfPunctuation++; // if !, incriments 
      } // closes if 
    } // closes for loop
    
    // find index of !
    int charBegin = 0; // intitializes charBegin
    int charEnd = 0; // initializes charEnd
    String stringPrevious = ""; // initializes StringPrevious
    String newString = ""; // initializes newString
    for (int loopNumber = 0; loopNumber <= numOfPunctuation; loopNumber++) {  
      if (loopNumber != 0) {
        charBegin = charEnd + 1; // if not first loop, charBegin is equal to position of ! at end of last loop + 1
      } // closes if 
      else {
        charBegin = 0; // if first loop, charBegin = 0
      } // closes else 
      // return location of ! after word
      int afterCharBegin = charBegin + 1; // gets number after charBegin to determine where to start charEnd
      if (loopNumber == numOfPunctuation) {
        charEnd = numCharactersMethod; // if final loop, charEnd = last character
      } // closes if 
      else {
        charEnd = sampleTextMethod.indexOf('?', afterCharBegin); // finds location of charEnd if not last word 
      } // closes else 
      // identify string within !s
      String stringInString = sampleTextMethod.substring(charBegin, charEnd); // identifies word based on character locations
      // only add . to end of sentence if loopNumber < numOf Exclamations 
      if (loopNumber < numOfPunctuation) {
        newString = stringPrevious + stringInString + ""; 
      } // closes if 
      else if (loopNumber == numOfPunctuation) {
        newString = stringPrevious + stringInString; 
      } // closes else if 
      // add string to growing string 
      stringPrevious = newString; 
      } // closes for  
    return newString; // returns new string
    } // closes findExclamation method
  
  public static String formatText(String sampleTextMethodBefore1) {
    // removes all punction and double spaces by calling on other methods 
    String sampleTextMethodBefore2 = deleteExclamation(sampleTextMethodBefore1); // gets rid of exclamations
    String sampleTextMethodBefore3 = deleteCommas(sampleTextMethodBefore2); // gets rid of commas 
    String sampleTextMethodBefore4 = deleteSemis(sampleTextMethodBefore3); // gets rid of semicolons
    String sampleTextMethodBefore5 = deletePunctuation(sampleTextMethodBefore4); // gets rid of periods
    String sampleTextMethodBefore6 = deleteQuestionMarks(sampleTextMethodBefore5); // gets rid of question marks 
    String sampleTextMethod = shortenSpace(sampleTextMethodBefore6); // gets rid of double spaces 
    return sampleTextMethod; // returns simplified text
  } // closes formatText
  
} // closes class 
