/////////////////
// Jannah Wing
// CSE 002- 010 
// Lab 09: Passing Arrays Inside Methods 


public class lab09{
  public static void main (String[] args) {
    // declare literal integer of length 8 
    int[] array0 = new int[] {1,2,3,4,5,6,7,8}; 
    
    // make two copies of array0
    int[] array1 = copy(array0);
    int[] array2 = copy(array0); 
    
    // pass array0 to inverter and print 
    System.out.println("Array0: ");
    inverter(array0);
    print(array0); 
    
    // pass array1 to inverter2 and print 
    System.out.println("Array1: ");
    inverter2(array1); 
    print(array1); 
    
    // pass array2 to inverter 2, assign to array3, and print array3
    System.out.println("Array3: "); 
    int[] array3 = inverter2(array2); 
    print(array3); 
  } // closes main 
  
  public static int[] copy(int[] myArray) {
    int[] copiedArray = new int[myArray.length]; 
    for (int i = 0; i < myArray.length; i++){ 
      copiedArray[i] = myArray[i]; 
    } // closes for 
    return copiedArray; 
  } // closes copy method
  
  public static void inverter(int[] myArray) {
    int maxIndexArray = myArray.length - 1; 
    int halfArray = myArray.length / 2; 
    int[] placeHolder = new int[1]; 
    for (int i = 0; i < halfArray; i++){ 
      placeHolder[0] = myArray[i]; 
      myArray[i] = myArray[maxIndexArray - i];
      myArray[maxIndexArray - i] = placeHolder[0]; 
    } // closes for 
  } // closes inverter method
  
  public static int[] inverter2(int[] myArray) {
    int[] newArray = new int[myArray.length];
    int maxIndexArray = myArray.length - 1; 
    for (int i = 0; i < myArray.length; i++){ 
      newArray[i] = myArray[maxIndexArray - i];
    } // closes for 
    return newArray; 
  } // closes inverter2 method 
  
  public static void print(int[] myArray) {
    for (int i = 0; i < myArray.length; i++){ 
      System.out.print(myArray[i]+" ");
    } // closes for 
    System.out.println();
  } // closes print method 
  
} // closes public class 