////////////////////
// Jannah Wing
// CSE 002
// HW Assignment #2: Arithmetic
// Due September 11, 2018
// 
// Computes the costs of items bought during shopping trip, including PA sales tax of 6%
//
// Input variables
//     Cost of pants, sweatshirts, and belts
//     Pairs of pants, numbers of sweatshirts, numbers of belts 
//
// Output variables: printed statements including
//     Total cost of pants, sweatshirts, and belts
//     Total tax for pants, sweatshirts, and belts
//     Total cost of items, sales tax, and purchase including tax
//
// define a class
public class Arithmetic{
  // add main method
  public static void main(String args[]){
    
    // declare and define input variables 
    int numPants = 3; // number of pairs of pants 
    double pantsPrice = 34.98; // cost per pair of pants 
    
    int numShirts = 2; // number of sweatshirts 
    double shirtPrice = 24.99; //cost per sweatshirt 
    
    int numBelts = 1; //number of belts
    double beltCost = 33.99; //cost per belt 
    
    double paSalesTax = 0.06; // the tax rate 
    
    // declare variables for output values 
    double totalPants; // total cost of pants 
    double totalShirts; // total cost of sweatshirts
    double totalBelts; // total cost of belts 
    
    double taxPants; // total sales tax charged on pants 
    double taxShirts; // total sales tax charged on sweatshirts 
    double taxBelts; // total sales tax charged on belts
      
    double totalPurchase; // total cost of purchase before tax
    double totalSalesTax; // total cost of sales tax 
    double totalPaid; // total cost for transaction, including sales tax 
    
    // calculate total costs of each item before tax
    totalPants = numPants * pantsPrice; // calculate total pants cost
    totalShirts = numShirts * shirtPrice; // calculate total sweatshirts cost
    totalBelts = numBelts * beltCost; // calculate total belts cost
    
    // calculate total tax charged buying each kind of item 
    taxPants = paSalesTax * totalPants; // calculate total tax for pants
    taxShirts = paSalesTax * totalShirts; // calculate total tax for sweatshirts 
    taxBelts = paSalesTax * totalBelts; // calculates total tax for belts 
    
    // calculate total cost of purchase
    totalPurchase = totalPants + totalShirts + totalBelts; // calculate total pre-tax cost
    totalSalesTax = taxPants + taxShirts + taxBelts; // calculate total sales tax
    totalPaid = totalPurchase + totalSalesTax; // calculate total paid for transaction 
    
    // convert output answers to two decimal places (removes excess digits but does not round)
    // by taking answer, multiplying by 100, converting to int, and dividing result by 100.0
    
    totalPants = (int) (100 * totalPants);
    totalPants /= 100.0; // converting total cost of pants to two decimal places
    
    totalShirts = (int) (100 * totalShirts);
    totalShirts /= 100.0; // converting total cost of sweatshirts to two decimal places
    
    totalBelts = (int) (100 * totalBelts);
    totalBelts /= 100.0; // converting total cost of belts to two decimal places
    
    taxPants = (int) (100 * taxPants);
    taxPants /= 100.0; // converting total tax on pants to two decimal places
    
    taxShirts = (int) (100 * taxShirts);
    taxShirts /= 100.0; // converting total tax on sweatshirts to two decimal places
    
    taxBelts = (int) (100 * taxBelts);
    taxBelts /= 100.0; // converting total tax on belts to two decimal places
    
    totalPurchase = (int) (100 * totalPurchase);
    totalPurchase /= 100.0; // converting total price, pre sales tax, to two decimal places
    
    totalSalesTax = (int) (100 * totalSalesTax);
    totalSalesTax /= 100.0; // converting total sales tax to two decimal places
    
    totalPaid = (int) (100 * totalPaid);
    totalPaid /= 100.0; // converting total price to two decimal places
    
    // print output data 
    System.out.println("The total cost of pants is $" + totalPants); 
    System.out.println("The total cost of sweatshirts is $" + totalShirts); 
    System.out.println("The total cost of belts is $" + totalBelts); 
    System.out.println("The total tax for pants is $" + taxPants); 
    System.out.println("The total tax for sweatshirts is $" + taxShirts); 
    System.out.println("The total tax for belts is $" + taxBelts); 
    System.out.println("The total purchase price, before sales tax, is $" + totalPurchase); 
    System.out.println("The total sales tax for the entire purchase is $" + totalSalesTax); 
    System.out.println("The total paid, including sales tax, is $" + totalPaid); 
    
  } // closes main
  
} // closes class 
      