////////////////////
// Jannah Wing
// CSE 002
// Lab #7: Methods
// Due November 2, 2018
// 
// Prints out random sentences 
//      Phase 1: Random Sentence with option to print more random sentences 
//      Phase 2: Paragraph with random number of supporting sentences 

// import random number generator class 
import java.util.Random;
// import Scanner class 
import java.util.Scanner; 
// define a class
public class Methods {
  
  // add main method required for every Java program
  public static void main(String [] args) {
    // define instance of scanner 
    Scanner myScanner = new Scanner(System.in); 
    
    // Phase 1
    System.out.println("Phase 1"); 
    // loop to generate random senternce + ask if want another random sentence 
    boolean enterLoop = true; // define boolean to enter loop
    while (enterLoop){
      System.out.println("The " + Adjective() + " " + Adjective() + " " + Subject() + " " + Verb() + " the " + Adjective() + " " + Objectt() + "."); 
      boolean enterInnerLoop = true; // define boolean to enter innerLoop 
      while (enterInnerLoop) {
        System.out.print("Would you like another random sentence generated? Type 0 for yes and 1 for no: "); 
        boolean intCheck = myScanner.hasNextInt(); 
        if (intCheck) { // if int entered 
          int enterLoopResponse = myScanner.nextInt(); 
          if (enterLoopResponse == 0) {
            enterInnerLoop = false; 
            enterLoop = true; // reenter loop to generate new sentence 
          }
          else if (enterLoopResponse == 1) {
            enterInnerLoop = false;
            enterLoop = false; // do not generate new sentence 
          }
          else { // if number not 1 or 0 
            System.out.println("Incorrect range. Please try again");
            enterInnerLoop = true;
          }
        } // closes else if 
        else { // if int not entered 
          myScanner.next(); // throws out incorrect data entry 
          System.out.println("Incorrect input. Please try again");
          enterInnerLoop = true; 
        } // closes else 
      } // closes while enterInnerLoop 
    } // closes while enterLoop 
    
    // Phase 2 
    System.out.println("Phase 2"); 
    
    String subjectMain = Thesis(); // prints thesis and defines subject 
    
    // print between 1 and 5 supporting sentences 
    Random randomGenerator = new Random();
    int randomNumberSentences = randomGenerator.nextInt(4) + 1; // random number of supporting sentences 1-5
    int sentenceGenerated = 1; 
    while (sentenceGenerated <= randomNumberSentences) {
      Supporting(subjectMain); // prints supporting
      sentenceGenerated++; // increments sentence generated 
    }// closes while 
    
    Conclusion(subjectMain); // print conclusion
    
  } // closes main
  
  // create method for generating random adjective 
  public static String Adjective () {
    // gets random integer less than 10 
    Random randomGenerator = new Random(); 
    int randomAdjectiveMethod = randomGenerator.nextInt(10);  
    // redefine numbers as words
    switch (randomAdjectiveMethod) {
      case 0: 
        return "Colorful";
      case 1:
        return "Cold"; 
      case 2: 
        return "Big";
      case 3:
        return "Small";
      case 4: 
        return "New"; 
      case 5: 
        return "Old";
      case 6:
        return "Fat"; 
      case 7: 
        return "Boring";
      case 8:
        return "Nice";
      case 9: 
        return "Pretty"; 
      default: 
        return "Incorrect Random Number Generated"; // check 
    } // closes Switch 
  } // closes Adjective 
  
  // create method for generating random subject 
  public static String Subject () {
    // gets random integer less than 10 
    Random randomGenerator = new Random(); 
    int randomSubjectMethod = randomGenerator.nextInt(10); // gets random number between 0-9
    // redefine numbers as words 
    switch (randomSubjectMethod) {
      case 0: 
        return "Cat";
      case 1:
        return "Dog"; 
      case 2: 
        return "Man";
      case 3:
        return "Boy";
      case 4: 
        return "Girl"; 
      case 5: 
        return "Woman";
      case 6:
        return "Child"; 
      case 7: 
        return "Baby";
      case 8:
        return "Doctor";
      case 9: 
        return "Professor"; 
      default: 
        return "Incorrect Random Number Generated"; // check 
    } // closes Switch 
  } // closes Subject 
  
  // create method for generating random verb
  public static String Verb () {
    // gets random integer less than 10 
    Random randomGenerator = new Random(); 
    int randomVerbMethod = randomGenerator.nextInt(10);  // gets random number between 0-9
    // redefine numbers as words
    switch (randomVerbMethod) {
      case 0: 
        return "Ate";
      case 1:
        return "Made"; 
      case 2: 
        return "Cooked";
      case 3:
        return "Prepared";
      case 4: 
        return "Bought"; 
      case 5: 
        return "Borrowed";
      case 6:
        return "Enjoyed"; 
      case 7: 
        return "Fried";
      case 8:
        return "Lost";
      case 9: 
        return "Returned"; 
      default: 
        return "Incorrect Random Number Generated"; // check 
    } // closes Switch 
  } // closes Verb
  
  // create method for generating random verb
  public static String Objectt () {
    // gets random integer less than 10 
    Random randomGenerator = new Random(); 
    int randomObjectMethod = randomGenerator.nextInt(10);  // gets random number between 0-9
    // redefine numbers as words
    switch (randomObjectMethod) {
      case 0: 
        return "Food";
      case 1:
        return "Salad"; 
      case 2: 
        return "Cheese";
      case 3:
        return "Cereal";
      case 4: 
        return "Snack"; 
      case 5: 
        return "Lunch";
      case 6:
        return "Pizza"; 
      case 7: 
        return "Pasta";
      case 8:
        return "Sandwich";
      case 9: 
        return "Bread"; 
      default: 
        return "Incorrect Random Number Generated"; // check 
    } // closes Switch 
  } // closes Object 
  
  // create method for generating random thesis sentences 
  public static String Thesis() {
    String subjectThesis = Subject(); 
    System.out.print("The " + Adjective() + " " + Adjective() + " " + subjectThesis + " " + Verb() + " the " + Adjective() + " " + Objectt() + ". ");
    return subjectThesis;
  } // closes Thesis 
  
  // create method for generating random supporting sentence 
  public static void Supporting(String subjectSupporting) {
    // get random integer less than 2
    Random randomGenerator = new Random(); 
    int itOrSubject = randomGenerator.nextInt(2); 
    if (itOrSubject == 1) {
      System.out.print("This " + subjectSupporting + " was very " + Adjective() + " and " + Verb() + " the " + Objectt() + ". "); 
      return;
    }
    else if (itOrSubject == 0) {
      System.out.print("It was very " + Adjective() + " and " + Verb() + " the " + Objectt() + ". ");
      return;
    }
    else {
      System.out.print("Recheck random number generator. "); // check 
    }
     
  } // closes Supporting
  
  // create method for generating concluding sentence 
  public static void Conclusion(String subjectConclusion) {
    System.out.println("Finally, that " + subjectConclusion + " " + Verb() + " her " + Objectt() + ". ");
    return; 
  } // closes Conclusion
  
} // closes class 