////////////////////
// Jannah Wing
// CSE 002
// HW Lab #2: Cyclometer
// Due September 11, 2018
// 
// Prints bicylce cyclometer information
//
// Input variables: obtained from bicycle cyclometer inlcuding 
//     Time elapsed in seconds
//     Number of rotations of the front wheel during that time
//
// Output variables: printed statements including
//     The number of minutes for each trip
//     The number of counts for each trip
//     The distance of each trip in miles
//     The distance for the two trips combined

// define a class
public class Cyclometer{
  // add main method required for every Java program
  public static void main(String args[]){
    
    // declare and define input variables 
    int secsTrip1 = 480; // number of seconds for first trip
    int secsTrip2 = 3220; // number of seconds for second trip 
    int countsTrip1 = 1561; // number of counts (rotations) for first trip
    int countsTrip2 = 9037; // number of counts (rotations) for second trip
    
    // declare and define variables for useful constants and intermediates
    double wheelDiameter = 27.0, // diameter of front wheel in inches
  	PI = 3.14159, // value of constant pi
  	feetPerMile = 5280, // conversion of feet per mile
  	inchesPerFoot = 12, // conversion of inches per foot
  	secondsPerMinute = 60; // conversion of seconds per minute 
    
    // declare variable for output statement 
   	double distanceTrip1, distanceTrip2, totalDistance; // 

    // print out number of minutes and counts for each trip
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + 
                       " minutes and had "+ countsTrip1 + " counts.");
                       // prints out number of minutes and counts for trip 1
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) +
                       " minutes and had " + countsTrip2 + " counts.");
                       // prints out number of minutes and counts for trip 2    
    
    /* calculate and store values for each distance using equation for circumference: 
       distance = counts * diameter * PI
       (for each count, a rotation of the wheel travels the diameter in inches times PI) */
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; 
        // calculates distance for trip 1 in inches
    distanceTrip1 /= inchesPerFoot * feetPerMile; 
        // converts trip 1 distance to miles 
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; 
        //calculates distance for trip 2 and converts to miles
    totalDistance = distanceTrip1 + distanceTrip2; 
        // calculates total distance in miles

    // print out distance output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles"); 
        // prints distance of trip 1
    System.out.println("Trip 2 was " + distanceTrip2 + " miles"); 
        // prints distance of trip 2
    System.out.println("The total distance was " + totalDistance + " miles"); 
        // prints total distance of trip 1 and 2 
 
  } // end of main method
  
} // end of class