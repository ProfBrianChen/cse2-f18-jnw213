////////////////////
// Jannah Wing
// CSE 002
// HW Assignment #1: Welcome Class
// Due September 4, 2018
// 
// Prints a welcome message to terminal, including: 
//     Welcome
//     Lehigh Network ID (jnw213)
//     Tweet-length autobiographic statement 
//
// define a class
public class WelcomeClass{
  // add main method
  public static void main(String args[]){
    // print a 3-line welcome message
    System.out.println("  -----------");
    System.out.println("  | WELCOME |");
    System.out.println("  -----------");
    // print 5-line message with Lehigh Network ID
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-j--n--w--2--1--3->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
    // print tweet-length autobiographic statement 
    System.out.println("I am a 5th-year IDEAS student");
    System.out.println("studying bionengineering and global health.");
    System.out.println("My goal is to work as a physician.");
                 
  }
  
}