/////////////////
// Jannah Wing
// CSE 002- 010 
// HW 09: Remove Elements 
//
// Creates array with random ints, removes elements of given index and value

import java.util.Scanner;

public class RemoveElements{ 
  public static void main(String [] arg) { // copied & edited from HW 09 on CSE2 coursesite
    Scanner scan = new Scanner(System.in);
    int num[] = new int[10];
    int newArray1[];
    int newArray2[];
    int index,target;
    String answer = "";
    do {
      System.out.println("Random input 10 ints [0-9]: ");
      num = randomInput();
      String out = "The original array is:";
      out += listArray(num);
      System.out.println(out);
      System.out.print("Enter the index ");
      index = scan.nextInt();
        
      newArray1 = delete(num,index);
      String out1 = "The output array is ";
      out1 += listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
 
      System.out.print("Enter the target value ");
      target = scan.nextInt();
      newArray2 = remove(num,target);
      String out2 = "The output array is ";
      out2 += listArray(newArray2); //return a string of the form "{2, 3, -9}"  
      System.out.println(out2);
  	 
      System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
      answer = scan.next();
    } while(answer.equals("Y") || answer.equals("y"));
  } // closes main method 
 
  public static String listArray(int num[]) { // copied & edited from HW 09 on CSE2 coursesite
    String out = "{";
    for(int j = 0; j < num.length; j++) {
      if(j > 0){
        out += ", ";
      } // closes if 
      out += num[j];
    } // closes for 
    out += "} ";
    return out;
  } // closes listArray method 
  
  public static int[] randomInput() { // assigns random ints to array
    int[] newArray = new int[10]; 
    for (int i = 0; i < 10; i++) { // cycles through 0-9 index of array
      int random = (int)(Math.random()*(9 + 1)); // random int between 0-9
      newArray[i] = random; 
    } // closes for 
    return newArray; 
  } // closes randomInput method
  
  public static int[] delete(int[] list, int pos) {
    int[] newArray; 
    if ((pos < 0) || (pos >= list.length)) { // if out of range 
      System.out.println("The index is not valid");
      newArray = new int[list.length];
      for (int i = 0; i < list.length; i++) {
        newArray[i] = list[i]; // print original array 
      } // closes for 
    } // closes if
    else { // if index within range 
      System.out.println("Index " + pos + " element is removed");
      newArray = new int[list.length - 1];
      for (int i = 0; i < pos; i++) { // assigns elemets before given position
        newArray[i] = list[i]; 
      } // closes for
      for (int k = pos; k < list.length-1; k++) { // assigns elements after given position
        newArray[k] = list[k + 1]; 
      } // closes for
    } // closes else
    return newArray; 
  } // closes delete method
  
  public static int[] remove(int[] list, int target) { // removes element of given value 
    int targetCount = 0; 
    // count elements with target value 
    for (int i = 0; i < list.length; i++) {
      if (list[i] == target) {
        targetCount++;
      } // closes if 
    } // closes for 
    if (targetCount > 0) {
      System.out.println("Element " + target + " has been found");
    } // closes if 
    else {
      System.out.println("Element " + target + " has not been found");
    } // closes else
    // create new array without target elements 
    int elements = list.length - targetCount;
    int[] newArray = new int[elements];
    int newArrayElement = 0; // keep track of new array indices 
    for (int k = 0; k < list.length; k++) {
      if (list[k] != target) {
        newArray[newArrayElement] = list[k];
        newArrayElement++; // only increment new array indices if needed 
      } // closes if 
    } // closes for 
    return newArray; 
  } // closes remove method
  
} // closes public class 

