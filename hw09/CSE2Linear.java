/////////////////
// Jannah Wing
// CSE 002- 010 
// HW 09: CSELinear
//
// Allows user to enter grades & grades to search for 
// Performs
//    Binary Search
//    Scrambles grades
//    Linear Search 

import java.util.Scanner;

public class CSE2Linear{
 
  public static void main (String[] args) { 
    // declare an instance of the Scanner object 
    Scanner myScanner = new Scanner (System.in);
    
    // get 15 ascending ints for students' grades 
    int[] studentGrades = new int[15];
    System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
    int i = 0; 
    while (i < 15) {
      boolean inputCheck = false; 
      while (!inputCheck) { 
        inputCheck = myScanner.hasNextInt(); 
        // if input type 
        if (inputCheck){ 
          int inputInt = myScanner.nextInt(); 
          // if correct range
          if ((inputInt >= 0) && (inputInt <= 100)) {
            // if meets all requirments, save as next element 
            // if first element 
            if (i == 0) {
              studentGrades[i] = inputInt; 
              i++; 
            } // closes if i == 0 
            // if ascending 
            else if (studentGrades[i-1] <= inputInt) {
              studentGrades[i] = inputInt; 
              i++; 
            } // closes if 
            else {
              System.out.println("Not ascending. Please enter integer greater than " + studentGrades[i-1] + ". ");
            } // closes else 
          } // closes if 
          else {
            System.out.println("Incorrect range. Please enter integer between 0-100.");
          } // closes else 
        } // closes if
        else if (!inputCheck) { 
          myScanner.next(); 
          System.out.println("Incorrect input. Please enter integer.");
        } // closes else 
      } // closes while
    } // closes for 
    
    // Print array 
    System.out.println("Array: "); 
    for (int k = 0; k < studentGrades.length; k++){ 
      System.out.print(studentGrades[k]+" ");
    } // closes for 
    System.out.println();
    
    // Binary search 
    System.out.print("Enter a grade to search for: "); 
    int key = myScanner.nextInt(); 
    binarySearch(studentGrades, key);
    
    // Scramle grades
    scrambling(studentGrades); 
    System.out.println("Scrambled: "); 
    print(studentGrades); 
    
    // Linear search scrambled grades 
    System.out.print("Enter a grade to search for: "); 
    int key2 = myScanner.nextInt(); 
    linearSearch(studentGrades, key2);
    
  } // closes main
  
  public static void print (int[] myArray) {
    for (int i = 0; i < myArray.length; i++){ 
      System.out.print(myArray[i]+" ");
    } // closes for 
    System.out.println();
  } // closes print method 
  
  public static void binarySearch (int[] myArray, int key) {
    // initialize 
    int low = 0; 
    int high = myArray.length-1; 
    int mid = (low + high) / 2; 
    boolean keyFound = false;
    int iterations = 0; 
    while (high >= low) {
      iterations++; 
      // if key < mid, search bothom half 
      if (key < myArray[mid]) {
        high = mid - 1; 
        mid = (low + high) / 2; 
      } // closes if 
      // if key = mid, indicate key found and stop loop 
      else if (key == myArray[mid]) {
        keyFound = true; 
        high = low - 1; // prevents from entering loop again
      } // closes else if 
      // if key > mid, search top half 
      else if (key > myArray[mid]) {
        low = mid + 1; 
        mid = (low + high) / 2; 
      } // closes else if 
    } // closes while 
    
    // print results
    if (keyFound) {
      System.out.println(key + " was found in the list with " + iterations + " iterations.");
    } // closes if 
    else if (!keyFound) {
      System.out.println(key + " was not found in the list with " + iterations + " iterations.");
    } // closes else if 
    
  } // closes binarySearch method 
  
  public static void scrambling (int[] myArray) {
    // create placeholder array
    int placeholder = 0; 
    // loop through shuffle 100 times 
    for (int i = 0; i < 100; i++){
      // Get randoms 
      int random = (int)(Math.random()*(myArray.length)); // gets random number between 0-14 
      // Switch elements 
      placeholder = myArray[0]; 
      myArray[0] = myArray[random];
      myArray[random] = placeholder; 
    } // closes for 
  } // closes scrambling method 
  
  public static void linearSearch (int[] myArray, int key) {
    boolean keyFound = false; 
    boolean stayInLoop = true; 
    int i = 0; 
    int iterations = 0;
    // loop until key found or last element reached 
    while (stayInLoop) {
      if (myArray[i] == key) {
        keyFound = true; 
        stayInLoop = false ;
      }
      iterations++; 
      i++;
      // leave loop if reached last element 
      if (i == myArray.length) {
        stayInLoop = false; 
      } // close if 
    } // closes while 
    
    // print results 
    if (keyFound) {
      System.out.println(key + " was found in the list with " + iterations + " iterations.");
    } // closes if 
    else if (!keyFound) {
      System.out.println(key + " was not found in the list with " + iterations + " iterations.");
    } // closes else if 
  } // closes linearSearch method 
  
} // closes public class 